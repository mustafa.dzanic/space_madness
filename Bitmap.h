#ifndef _BITMAP_H				// If we haven't included this file
#define _BITMAP_H				// Set a flag saying we included it
				// Include our main header file
#include "main.h"

void Render(HWND hwnd);

BOOL Initialize(void);

void Draw(HDC hdc, RECT* prect);

#endif
