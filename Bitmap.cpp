#include "bitmap.h"


void Render(HWND hwnd)
{

	RECT clientRectangle;
	HDC hdc = GetDC(hwnd);

	GetClientRect(hwnd, &clientRectangle);
	Draw(hdc, &clientRectangle);

	ReleaseDC(hwnd, hdc);
}

BOOL Initialize(void) {

	boss[0].numOfGreenSquares = 10;

	switch (Player1ship)
	{
	case 0:
		player1.numOfGreenSquares = 10;
		break;
	case 1:
		player1.numOfGreenSquares = 12;
		break;
	case 2:
		player1.numOfGreenSquares = 6;
		break;
	case 3:
		player1.numOfGreenSquares = 6;
		break;
	case 4:
		player1.numOfGreenSquares = 10;
		break;
	case 5:
		player1.numOfGreenSquares = 10;
		break;
	case 6:
		player1.numOfGreenSquares = 10;
		break;
	case 7:
		player1.numOfGreenSquares = 6;
		break;
	case 8:
		player1.numOfGreenSquares = 10;
		break;
	case 9:
		player1.numOfGreenSquares = 10;
		break;
	default:
		break;
	}

	switch (Player2ship)
	{
	case 0:
		player2.numOfGreenSquares = 10;
		break;
	case 1:
		player2.numOfGreenSquares = 12;
		break;
	case 2:
		player2.numOfGreenSquares = 6;
		break;
	case 3:
		player2.numOfGreenSquares = 6;
		break;
	case 4:
		player2.numOfGreenSquares = 10;
		break;
	case 5:
		player2.numOfGreenSquares = 10;
		break;
	case 6:
		player2.numOfGreenSquares = 10;
		break;
	case 7:
		player2.numOfGreenSquares = 6;
		break;
	case 8:
		player2.numOfGreenSquares = 10;
		break;
	case 9:
		player2.numOfGreenSquares = 10;
		break;
	default:
		break;
	}
	hbmBackground[0] = (HBITMAP)LoadImage(NULL, "home.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[1] = (HBITMAP)LoadImage(NULL, "1player.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[2] = (HBITMAP)LoadImage(NULL, "2players.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[3] = (HBITMAP)LoadImage(NULL, "credits.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[4] = (HBITMAP)LoadImage(NULL, "controls.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[5] = (HBITMAP)LoadImage(NULL, "loadGame.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[6] = (HBITMAP)LoadImage(NULL, "exitGame.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[7] = (HBITMAP)LoadImage(NULL, "controlsPage.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[8] = (HBITMAP)LoadImage(NULL, "creditsPage.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[9] = (HBITMAP)LoadImage(NULL, "playGame.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[10] = (HBITMAP)LoadImage(NULL, "insidePlay.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[11] = (HBITMAP)LoadImage(NULL, "selectShip.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[12] = (HBITMAP)LoadImage(NULL, "saveGame.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[13] = (HBITMAP)LoadImage(NULL, "mainMenu.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[14] = (HBITMAP)LoadImage(NULL, "escMenu.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[15] = (HBITMAP)LoadImage(NULL, "escMenuSelectShip.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[16] = (HBITMAP)LoadImage(NULL, "escMenuSaveGame.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[17] = (HBITMAP)LoadImage(NULL, "escMenuMainMenu.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[18] = (HBITMAP)LoadImage(NULL, "MDZPage.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[19] = (HBITMAP)LoadImage(NULL, "carrierPage.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[20] = (HBITMAP)LoadImage(NULL, "MagnumPage.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[21] = (HBITMAP)LoadImage(NULL, "SpaceBugPage.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[22] = (HBITMAP)LoadImage(NULL, "sharpPage.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[23] = (HBITMAP)LoadImage(NULL, "ejPage.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[24] = (HBITMAP)LoadImage(NULL, "tuzlaPage.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[25] = (HBITMAP)LoadImage(NULL, "fet14127Page.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[26] = (HBITMAP)LoadImage(NULL, "fet14129Page.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[27] = (HBITMAP)LoadImage(NULL, "marvelPage.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[28] = (HBITMAP)LoadImage(NULL, "levelComplete.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[36] = (HBITMAP)LoadImage(NULL, "gameover.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[29] = (HBITMAP)LoadImage(NULL, "Stage1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[30] = (HBITMAP)LoadImage(NULL, "Stage2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[31] = (HBITMAP)LoadImage(NULL, "Stage3.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[32] = (HBITMAP)LoadImage(NULL, "Stage4.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[33] = (HBITMAP)LoadImage(NULL, "Stage5.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[34] = (HBITMAP)LoadImage(NULL, "Stage6.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBackground[35] = (HBITMAP)LoadImage(NULL, "Stage7.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	//makebg(hbmBackground[0], background);

	BITMAP bitmap;
	hbmSpaceship[0] = (HBITMAP)LoadImage(NULL, "mdzMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmSpaceshipMask[0] = (HBITMAP)LoadImage(NULL, "mdz.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmSpaceship[0], sizeof(BITMAP), &bitmap);
	spaceship[0].width = bitmap.bmWidth / 7;
	spaceship[0].height = bitmap.bmHeight;
	spaceship[0].x = 500;
	spaceship[0].y = 500;
	spaceship[0].dx = 10;
	spaceship[0].dy = 10;

	hbmSpaceship[1] = (HBITMAP)LoadImage(NULL, "carrierMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmSpaceshipMask[1] = (HBITMAP)LoadImage(NULL, "carrier.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmSpaceship[1], sizeof(BITMAP), &bitmap);
	spaceship[1].width = bitmap.bmWidth / 4;
	spaceship[1].height = bitmap.bmHeight;
	spaceship[1].x = 600;
	spaceship[1].y = 500;
	spaceship[1].dx = 8;
	spaceship[1].dy = 8;

	hbmSpaceship[2] = (HBITMAP)LoadImage(NULL, "magnumMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmSpaceshipMask[2] = (HBITMAP)LoadImage(NULL, "magnum.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmSpaceship[2], sizeof(BITMAP), &bitmap);
	spaceship[2].width = bitmap.bmWidth / 7;
	spaceship[2].height = bitmap.bmHeight;
	spaceship[2].x = 700;
	spaceship[2].y = 500;
	spaceship[2].dx = 6;
	spaceship[2].dy = 6;

	hbmSpaceship[3] = (HBITMAP)LoadImage(NULL, "bugMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmSpaceshipMask[3] = (HBITMAP)LoadImage(NULL, "bug.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmSpaceship[3], sizeof(BITMAP), &bitmap);
	spaceship[3].width = bitmap.bmWidth / 6;
	spaceship[3].height = bitmap.bmHeight;
	spaceship[3].x = 800;
	spaceship[3].y = 500;
	spaceship[3].dx = 18;
	spaceship[3].dy = 18;

	hbmSpaceship[4] = (HBITMAP)LoadImage(NULL, "sharpMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmSpaceshipMask[4] = (HBITMAP)LoadImage(NULL, "sharp.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmSpaceship[4], sizeof(BITMAP), &bitmap);
	spaceship[4].width = bitmap.bmWidth / 4;
	spaceship[4].height = bitmap.bmHeight;
	spaceship[4].x = 900;
	spaceship[4].y = 500;
	spaceship[4].dx = 4;
	spaceship[4].dy = 4;

	hbmSpaceship[5] = (HBITMAP)LoadImage(NULL, "ejMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmSpaceshipMask[5] = (HBITMAP)LoadImage(NULL, "ej.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmSpaceship[5], sizeof(BITMAP), &bitmap);
	spaceship[5].width = bitmap.bmWidth / 8;
	spaceship[5].height = bitmap.bmHeight;
	spaceship[5].x = 500;
	spaceship[5].y = 600;
	spaceship[5].dx = 10;
	spaceship[5].dy = 10;

	hbmSpaceship[6] = (HBITMAP)LoadImage(NULL, "tuzlaMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmSpaceshipMask[6] = (HBITMAP)LoadImage(NULL, "tuzla.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmSpaceship[6], sizeof(BITMAP), &bitmap);
	spaceship[6].width = bitmap.bmWidth / 6;
	spaceship[6].height = bitmap.bmHeight;
	spaceship[6].x = 700;
	spaceship[6].y = 600;
	spaceship[6].dx = 10;
	spaceship[6].dy = 10;

	hbmSpaceship[7] = (HBITMAP)LoadImage(NULL, "fet14127Mask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmSpaceshipMask[7] = (HBITMAP)LoadImage(NULL, "fet14127.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmSpaceship[7], sizeof(BITMAP), &bitmap);
	spaceship[7].width = bitmap.bmWidth / 4;
	spaceship[7].height = bitmap.bmHeight;
	spaceship[7].x = 800;
	spaceship[7].y = 600;
	spaceship[7].dx = 18;
	spaceship[7].dy = 18;

	hbmSpaceship[8] = (HBITMAP)LoadImage(NULL, "fet14129Mask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmSpaceshipMask[8] = (HBITMAP)LoadImage(NULL, "fet14129.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmSpaceship[8], sizeof(BITMAP), &bitmap);
	spaceship[8].width = bitmap.bmWidth / 8;
	spaceship[8].height = bitmap.bmHeight;
	spaceship[8].x = 900;
	spaceship[8].y = 600;
	spaceship[8].dx = 12;
	spaceship[8].dy = 12;

	hbmSpaceship[9] = (HBITMAP)LoadImage(NULL, "marvelMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmSpaceshipMask[9] = (HBITMAP)LoadImage(NULL, "marvel.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmSpaceship[9], sizeof(BITMAP), &bitmap);
	spaceship[9].width = bitmap.bmWidth / 4;
	spaceship[9].height = bitmap.bmHeight;
	spaceship[9].x = 600;
	spaceship[9].y = 600;
	spaceship[9].dx = 16;
	spaceship[9].dy = 16;

	hbmFog = (HBITMAP)LoadImage(NULL, "fogMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmFogMask = (HBITMAP)LoadImage(NULL, "fog.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmFog, sizeof(BITMAP), &bitmap);
	fog.width = bitmap.bmWidth;
	fog.height = bitmap.bmHeight;
	fog.x = 0;
	fog.y = 0;
	fog.dx = 2;
	fog.dy = 1;

	hbmBoss[0] = (HBITMAP)LoadImage(NULL, "boss1Mask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBossMask[0] = (HBITMAP)LoadImage(NULL, "boss1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmSpaceship[7], sizeof(BITMAP), &bitmap);
	boss[0].width = 125;
	boss[0].height = 238;
	boss[0].x = 800;
	boss[0].y = 50;
	boss[0].dx = 5;
	boss[0].dy = 5;

	
	



	//makeStone(hbmStones[1], stones[1]);

	for (int j = 0; j < 10; j++)
	{
		hbmBullet[j] = (HBITMAP)LoadImage(NULL, "bullet1Mask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		hbmBulletMask[j] = (HBITMAP)LoadImage(NULL, "bullet1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		GetObject(hbmBullet[0], sizeof(BITMAP), &bitmap);
		bullet[j].width = bitmap.bmWidth;
		bullet[j].height = bitmap.bmHeight;
		bullet[j].dx = 0;
		bullet[j].dy = 20;

	}

	for (int v = 0; v < 10; v++)
	{
		hbmBulletBoost[v] = (HBITMAP)LoadImage(NULL, "bullet2Mask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		hbmBulletBoostMask[v] = (HBITMAP)LoadImage(NULL, "bullet2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		GetObject(hbmBulletBoost[0], sizeof(BITMAP), &bitmap);
		bulletBoost[v].width = bitmap.bmWidth;
		bulletBoost[v].height = bitmap.bmHeight;
		bulletBoost[v].dx = -20;
		bulletBoost[v].dy = -20;

	}

	for (int w = 0; w < 10; w++)
	{
		hbmBulletMagnum[w] = (HBITMAP)LoadImage(NULL, "bullet3Mask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		hbmBulletMagnumMask[w] = (HBITMAP)LoadImage(NULL, "bullet3.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		GetObject(hbmBulletMagnum[0], sizeof(BITMAP), &bitmap);
		bulletMagnum[w].width = bitmap.bmWidth;
		bulletMagnum[w].height = bitmap.bmHeight;
		bulletMagnum[w].dx = 0;
		bulletMagnum[w].dy = 20;
	}
	for (int wq = 0; wq < 50; wq++)
	{
		hbmStonesSteel[wq] = (HBITMAP)LoadImage(NULL, "asteroid1Mask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		hbmStonesSteelMask[wq] = (HBITMAP)LoadImage(NULL, "asteroid1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		GetObject(hbmStonesSteel[wq], sizeof(BITMAP), &bitmap);
		stonesSteel[wq].width = bitmap.bmWidth / 8;
		stonesSteel[wq].height = bitmap.bmHeight / 8;
		stonesSteel[wq].y = -bitmap.bmHeight / 8;
		stonesSteel[wq].dx = rand() % levels[currentLevel].upperBoundSpeed;
		stonesSteel[wq].dy = rand() % (levels[currentLevel].upperBoundSpeed - levels[currentLevel].lowerBoundSpeed) + levels[currentLevel].lowerBoundSpeed;
	}
	for (int qw = 0; qw < 50; qw++)
	{
		hbmStonesGround[qw] = (HBITMAP)LoadImage(NULL, "asteroid2Mask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		hbmStonesGroundMask[qw] = (HBITMAP)LoadImage(NULL, "asteroid2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		GetObject(hbmStonesGround[qw], sizeof(BITMAP), &bitmap);
		stonesGround[qw].width = bitmap.bmWidth / 8;
		stonesGround[qw].height = bitmap.bmHeight / 8;
		stonesGround[qw].y = -bitmap.bmHeight / 8;
		stonesGround[qw].dx = rand() % levels[currentLevel].upperBoundSpeed;
		stonesGround[qw].dy = rand() % (levels[currentLevel].upperBoundSpeed - levels[currentLevel].lowerBoundSpeed) + levels[currentLevel].lowerBoundSpeed;
	}

	for (int ex = 0; ex < 10; ex++)
	{
		hbmExplosion[ex] = (HBITMAP)LoadImage(NULL, "explosion2Mask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		hbmExplosionMask[ex] = (HBITMAP)LoadImage(NULL, "explosion2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		GetObject(hbmExplosion[ex], sizeof(BITMAP), &bitmap);
		explosion[ex].width = bitmap.bmWidth / 4;
		explosion[ex].height = bitmap.bmHeight / 4;
		explosion[ex].x = 0;
		explosion[ex].y = 0;
		explosion[ex].dx = 0;
		explosion[ex].dy = 0;

	}

	hbmShieldPlayer1 = (HBITMAP)LoadImage(NULL, "shieldOnPlayerMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmShieldPlayer1Mask = (HBITMAP)LoadImage(NULL, "shieldOnPlayer.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmShieldPlayer1, sizeof(BITMAP), &bitmap);
	shieldOnPlayer1.width = bitmap.bmWidth;
	shieldOnPlayer1.height = bitmap.bmHeight;

	hbmShieldPlayer2 = (HBITMAP)LoadImage(NULL, "shieldOnPlayerMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmShieldPlayer2Mask = (HBITMAP)LoadImage(NULL, "shieldOnPlayer.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmShieldPlayer2, sizeof(BITMAP), &bitmap);
	shieldOnPlayer2.width = bitmap.bmWidth;
	shieldOnPlayer2.height = bitmap.bmHeight;

	hbmHealthbarL = (HBITMAP)LoadImage(NULL, "healthbarMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmHealthbarLMask = (HBITMAP)LoadImage(NULL, "healthbar.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmHealthbarL, sizeof(BITMAP), &bitmap);
	healthbarL.width = bitmap.bmWidth;
	healthbarL.height = bitmap.bmHeight;
	healthbarL.x = 10;
	healthbarL.y = 10;

	hbmHealthbarR = (HBITMAP)LoadImage(NULL, "healthbarMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmHealthbarRMask = (HBITMAP)LoadImage(NULL, "healthbar.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmHealthbarR, sizeof(BITMAP), &bitmap);
	healthbarR.width = bitmap.bmWidth;
	healthbarR.height = bitmap.bmHeight;
	healthbarR.x = 1910 - bitmap.bmWidth;
	healthbarR.y = 10;

	for (int b = 0; b < 10; b++)
	{
		hbmBossBullet[b] = (HBITMAP)LoadImage(NULL, "bulletBossMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		hbmBossBulletMask[b] = (HBITMAP)LoadImage(NULL, "bulletBoss.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		GetObject(hbmBossBullet[b], sizeof(BITMAP), &bitmap);
		bossBullet[b].width = bitmap.bmWidth;
		bossBullet[b].height = bitmap.bmHeight;
		bossBullet[b].dx = 0;
		bossBullet[b].dy = 20;

	}

	for (int greenx = 0; greenx < 24; greenx++)
	{
		hbmGreen = (HBITMAP)LoadImage(NULL, "green.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		GetObject(hbmGreen, sizeof(BITMAP), &bitmap);
		green[greenx].width = bitmap.bmWidth;
		green[greenx].height = bitmap.bmHeight;
		green[greenx].x = 1910 - bitmap.bmWidth;
		green[greenx].y = 10;
	}

	for (int greenx = 0; greenx < 24; greenx++)
	{
		hbmGreen = (HBITMAP)LoadImage(NULL, "green.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		GetObject(hbmGreen, sizeof(BITMAP), &bitmap);
		green2[greenx].width = bitmap.bmWidth;
		green2[greenx].height = bitmap.bmHeight;
		green2[greenx].x = -100;
		green2[greenx].y = -100;
	}
	
	hbmBoost[0] = (HBITMAP)LoadImage(NULL, "attack.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmBoost[0], sizeof(BITMAP), &bitmap);
	boostObj[0].width = bitmap.bmWidth;
	boostObj[0].height = bitmap.bmHeight;
	boostObj[0].x = rand() % 1920;
	boostObj[0].y = -100;

	hbmBoost[1] = (HBITMAP)LoadImage(NULL, "defense.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmBoost[1], sizeof(BITMAP), &bitmap);
	boostObj[1].width = bitmap.bmWidth;
	boostObj[1].height = bitmap.bmHeight;
	boostObj[1].x = rand() % 1920;
	boostObj[1].y = -100;

	hbmBoost[2] = (HBITMAP)LoadImage(NULL, "speed.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmBoost[2], sizeof(BITMAP), &bitmap);
	boostObj[2].width = bitmap.bmWidth;
	boostObj[2].height = bitmap.bmHeight;
	boostObj[2].x = rand() % 1920;
	boostObj[2].y = -100;

	hbmBoost[3] = (HBITMAP)LoadImage(NULL, "shield.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBoostMask = (HBITMAP)LoadImage(NULL, "shieldMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmBoost[3], sizeof(BITMAP), &bitmap);
	boostObj[3].width = bitmap.bmWidth;
	boostObj[3].height = bitmap.bmHeight;
	boostObj[3].x = rand() % 1920;
	boostObj[3].y = -100;

	hbmBoost[4] = (HBITMAP)LoadImage(NULL, "health.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmBoost[4], sizeof(BITMAP), &bitmap);
	boostObj[4].width = bitmap.bmWidth;
	boostObj[4].height = bitmap.bmHeight;
	boostObj[4].x = rand() % 1920;
	boostObj[4].y = -100;

	hbmScore = (HBITMAP)LoadImage(NULL, "score.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	hbmBoostMenuPlayer1[0] = (HBITMAP)LoadImage(NULL, "attackMenu.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmBoostMenuPlayer1[0], sizeof(BITMAP), &bitmap);
	boostMenuPlayer1[0].width = bitmap.bmWidth;
	boostMenuPlayer1[0].height = bitmap.bmHeight;
	boostMenuPlayer1[0].x = 25;
	boostMenuPlayer1[0].y = 940;

	hbmBoostMenuPlayer1[1] = (HBITMAP)LoadImage(NULL, "defenseMenu.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmBoostMenuPlayer1[1], sizeof(BITMAP), &bitmap);
	boostMenuPlayer1[1].width = bitmap.bmWidth;
	boostMenuPlayer1[1].height = bitmap.bmHeight;
	boostMenuPlayer1[1].x = 112;
	boostMenuPlayer1[1].y = 940;

	hbmBoostMenuPlayer1[2] = (HBITMAP)LoadImage(NULL, "speedMenu.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmBoostMenuPlayer1[2], sizeof(BITMAP), &bitmap);
	boostMenuPlayer1[2].width = bitmap.bmWidth;
	boostMenuPlayer1[2].height = bitmap.bmHeight;
	boostMenuPlayer1[2].x = 199;
	boostMenuPlayer1[2].y = 940;

	hbmBoostMenuPlayer1[3] = (HBITMAP)LoadImage(NULL, "shieldMenu.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBoostMenuMask[0] = (HBITMAP)LoadImage(NULL, "shieldMenuMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmBoostMenuPlayer1[3], sizeof(BITMAP), &bitmap);
	boostMenuPlayer1[3].width = bitmap.bmWidth;
	boostMenuPlayer1[3].height = bitmap.bmHeight;
	boostMenuPlayer1[3].x = 286;
	boostMenuPlayer1[3].y = 940;

	hbmBoostMenuPlayer1[4] = (HBITMAP)LoadImage(NULL, "healthMenu.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmBoostMenuPlayer1[4], sizeof(BITMAP), &bitmap);
	boostMenuPlayer1[4].width = bitmap.bmWidth;
	boostMenuPlayer1[4].height = bitmap.bmHeight;
	boostMenuPlayer1[4].x = 373;
	boostMenuPlayer1[4].y = 940;

	hbmBoostMenuPlayer2[0] = (HBITMAP)LoadImage(NULL, "attackMenu.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmBoostMenuPlayer2[0], sizeof(BITMAP), &bitmap);
	boostMenuPlayer2[0].width = bitmap.bmWidth;
	boostMenuPlayer2[0].height = bitmap.bmHeight;
	boostMenuPlayer2[0].x = 1285;
	boostMenuPlayer2[0].y = 940;

	hbmBoostMenuPlayer2[1] = (HBITMAP)LoadImage(NULL, "defenseMenu.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmBoostMenuPlayer2[1], sizeof(BITMAP), &bitmap);
	boostMenuPlayer2[1].width = bitmap.bmWidth;
	boostMenuPlayer2[1].height = bitmap.bmHeight;
	boostMenuPlayer2[1].x = 1372;
	boostMenuPlayer2[1].y = 940;

	hbmBoostMenuPlayer2[2] = (HBITMAP)LoadImage(NULL, "speedMenu.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmBoostMenuPlayer2[2], sizeof(BITMAP), &bitmap);
	boostMenuPlayer2[2].width = bitmap.bmWidth;
	boostMenuPlayer2[2].height = bitmap.bmHeight;
	boostMenuPlayer2[2].x = 1459;
	boostMenuPlayer2[2].y = 940;

	hbmBoostMenuPlayer2[3] = (HBITMAP)LoadImage(NULL, "shieldMenu.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmBoostMenuMask[1] = (HBITMAP)LoadImage(NULL, "shieldMenuMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmBoostMenuPlayer2[3], sizeof(BITMAP), &bitmap);
	boostMenuPlayer2[3].width = bitmap.bmWidth;
	boostMenuPlayer2[3].height = bitmap.bmHeight;
	boostMenuPlayer2[3].x = 1546;
	boostMenuPlayer2[3].y = 940;

	hbmBoostMenuPlayer2[4] = (HBITMAP)LoadImage(NULL, "healthMenu.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmBoostMenuPlayer2[4], sizeof(BITMAP), &bitmap);
	boostMenuPlayer2[4].width = bitmap.bmWidth;
	boostMenuPlayer2[4].height = bitmap.bmHeight;
	boostMenuPlayer2[4].x = 1633;
	boostMenuPlayer2[4].y = 940;

	hbmItemsPlayer1 = (HBITMAP)LoadImage(NULL, "items.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmItemsPlayer1Mask = (HBITMAP)LoadImage(NULL, "itemsMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmItemsPlayer1, sizeof(BITMAP), &bitmap);
	itemsPlayer1.width = bitmap.bmWidth;
	itemsPlayer1.height = bitmap.bmHeight;
	itemsPlayer1.x = 10;
	itemsPlayer1.y = 1050-itemsPlayer1.height;

	hbmItemsPlayer2 = (HBITMAP)LoadImage(NULL, "items.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmItemsPlayer2Mask = (HBITMAP)LoadImage(NULL, "itemsMask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmItemsPlayer2, sizeof(BITMAP), &bitmap);
	itemsPlayer2.width = bitmap.bmWidth;
	itemsPlayer2.height = bitmap.bmHeight;
	itemsPlayer2.x = 1910 - itemsPlayer2.width;
	itemsPlayer2.y = 1050 - itemsPlayer2.height;

	for (int j = 0; j < 10; j++)
	{
		hbmBulletPlayer2[j] = (HBITMAP)LoadImage(NULL, "bullet1Mask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		hbmBulletMaskPlayer2[j] = (HBITMAP)LoadImage(NULL, "bullet1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		GetObject(hbmBulletPlayer2[j], sizeof(BITMAP), &bitmap);
		bulletPlayer2[j].width = bitmap.bmWidth;
		bulletPlayer2[j].height = bitmap.bmHeight;
		bulletPlayer2[j].dx = 0;
		bulletPlayer2[j].dy = 20;
	}

	for (int v = 0; v < 10; v++)
	{
		hbmBulletBoostPlayer2[v] = (HBITMAP)LoadImage(NULL, "bullet2Mask.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		hbmBulletBoostMaskPlayer2[v] = (HBITMAP)LoadImage(NULL, "bullet2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		GetObject(hbmBulletBoostPlayer2[0], sizeof(BITMAP), &bitmap);
		bulletBoostPlayer2[v].width = bitmap.bmWidth;
		bulletBoostPlayer2[v].height = bitmap.bmHeight;
		bulletBoostPlayer2[v].dx = 0;
		bulletBoostPlayer2[v].dy = 20;
	}
	return TRUE;
}

void Draw(HDC hdc, RECT* prect)
{
	HDC hdcBuffer = CreateCompatibleDC(hdc);
	HBITMAP hbmBuffer = CreateCompatibleBitmap(hdc, prect->right, prect->bottom);
	HBITMAP hbmOldBuffer = (HBITMAP)SelectObject(hdcBuffer, hbmBuffer);
	
	
	for (Object kamencic : stonesSteel)
	{
		kamencic.x -= kamencic.dx;
		kamencic.y += kamencic.dy + 3;
	}

	if (fog.x > 2000)
		fog.x = -200;
	if (fog.y > 2000)
		fog.y = -200;

	HDC hdcMem = CreateCompatibleDC(hdc);
	HBITMAP hbmOld;
	switch (glBackGround)
	{
	case 0:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[0]);
		break;
	case 1:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[1]);
		break;
	case 2:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[2]);
		break;
	case 3:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[3]);
		break;
	case 4:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[4]);
		break;
	case 5:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[5]);
		break;
	case 6:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[6]);
		break;
	case 7:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[7]);
		break;
	case 8:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[8]);
		break;
	case 9:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[9]);
		break;
	case 10:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[10]);
		break;
	case 11:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[11]);
		break;
	case 12:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[12]);
		break;
	case 13:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[13]);
		break;
	case 14:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[14]);
		break;
	case 15:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[15]);
		break;
	case 16:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[16]);
		break;
	case 17:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[17]);
		break;
	case 18:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[18]);
		break;
	case 19:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[19]);
		break;
	case 20:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[20]);
		break;
	case 21:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[21]);
		break;
	case 22:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[22]);
		break;
	case 23:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[23]);
		break;
	case 24:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[24]);
		break;
	case 25:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[25]);
		break;
	case 26:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[26]);
		break;
	case 27:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[27]);
		break;
	case 28:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[28]);
		break;
	case 29:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[29]);
		break;
	case 30:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[30]);
		break;
	case 31:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[31]);
		break;
	case 32:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[32]);
		break;
	case 33:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[33]);
		break;
	case 34:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[34]);
		break;
	case 35:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[35]);
		break;
	case 36:
		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground[36]);
	default:
		cout << "jao";
	}



	BitBlt(hdcBuffer, 0, 0, 1920, 1080, hdcMem, 0, 0, SRCCOPY);


	//BitBlt(hdcBuffer, background.x, background.y, background.width, background.height, hdcMem, 0, 0, SRCCOPY);
	SelectObject(hdc, hbmOld);

	if (glBackGround > 28 && glBackGround < 36)
	{
		if (player2.attack) {
			while (bulletsOnScreen2Player2 > bulletsBoostPlayer2.size())
			{
				bulletsBoostPlayer2.push_back(bulletBoostPlayer2[bulletCounter2Player2]);
				bulletCounter2Player2++;
			}

			int z = 0;
			for (Object bulletBoostPlayer2 : bulletsBoostPlayer2)
			{

				(HBITMAP)SelectObject(hdcMem, hbmBulletBoostMaskPlayer2[z]);
				BitBlt(hdcBuffer, bulletBoostPlayer2.x, bulletBoostPlayer2.y, bulletBoostPlayer2.width, bulletBoostPlayer2.height, hdcMem, 0, 0, SRCAND);

				hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBulletBoostPlayer2[z]);
				BitBlt(hdcBuffer, bulletBoostPlayer2.x, bulletBoostPlayer2.y, bulletBoostPlayer2.width, bulletBoostPlayer2.height, hdcMem, 0, 0, SRCPAINT);

				z++;
			}
		}
		else {
			while (bulletsOnScreenPlayer2 > bulletsPlayer2.size())
			{
				bulletsPlayer2.push_back(bulletPlayer2[bulletCounterPlayer2]);
				bulletCounterPlayer2++;
			}
			int i = 0;
			for (Object bulletPlayer2 : bulletsPlayer2)
			{

				(HBITMAP)SelectObject(hdcMem, hbmBulletMaskPlayer2[i]);
				BitBlt(hdcBuffer, bulletPlayer2.x, bulletPlayer2.y, bulletPlayer2.width, bulletPlayer2.height, hdcMem, 0, 0, SRCAND);

				hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBulletPlayer2[i]);
				BitBlt(hdcBuffer, bulletPlayer2.x, bulletPlayer2.y, bulletPlayer2.width, bulletPlayer2.height, hdcMem, 0, 0, SRCPAINT);

				i++;
			}
		}
		while (bossBulletsOnScreen > bossBullets.size())
		{
			bossBullets.push_back(bossBullet[bossBulletCounter]);
			bossBulletCounter++;
		}
		int b = 0;
		for (Object bullet : bossBullets)
		{

			(HBITMAP)SelectObject(hdcMem, hbmBossBulletMask[b]);
			BitBlt(hdcBuffer, bullet.x, bullet.y, bullet.width, bullet.height, hdcMem, 0, 0, SRCAND);

			hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBossBullet[b]);
			BitBlt(hdcBuffer, bullet.x, bullet.y, bullet.width, bullet.height, hdcMem, 0, 0, SRCPAINT);

			b++;
		}

		(HBITMAP)SelectObject(hdcMem, hbmItemsPlayer1);
		BitBlt(hdcBuffer, itemsPlayer1.x, itemsPlayer1.y, itemsPlayer1.width, itemsPlayer1.height, hdcMem, 0, 0, SRCAND);

		HBITMAP hbmOld1 = (HBITMAP)SelectObject(hdcMem, hbmItemsPlayer1Mask);
		BitBlt(hdcBuffer, itemsPlayer1.x, itemsPlayer1.y, itemsPlayer1.width, itemsPlayer1.height, hdcMem, 0, 0, SRCPAINT);

		if (glPlayers == 2)
		{
			(HBITMAP)SelectObject(hdcMem, hbmItemsPlayer2);
			BitBlt(hdcBuffer, itemsPlayer2.x, itemsPlayer2.y, itemsPlayer2.width, itemsPlayer2.height, hdcMem, 0, 0, SRCAND);

			HBITMAP hbmOld1 = (HBITMAP)SelectObject(hdcMem, hbmItemsPlayer2Mask);
			BitBlt(hdcBuffer, itemsPlayer2.x, itemsPlayer2.y, itemsPlayer2.width, itemsPlayer2.height, hdcMem, 0, 0, SRCPAINT);
		}
		if (player1.attack)
		{
			(HBITMAP)SelectObject(hdcMem, hbmBoostMenuPlayer1[0]);
			BitBlt(hdcBuffer, boostMenuPlayer1[0].x, boostMenuPlayer1[0].y, boostMenuPlayer1[0].width, boostMenuPlayer1[0].height, hdcMem, 0, 0, SRCPAINT);
		}
		if (player1.defense)
		{
			(HBITMAP)SelectObject(hdcMem, hbmBoostMenuPlayer1[1]);
			BitBlt(hdcBuffer, boostMenuPlayer1[1].x, boostMenuPlayer1[1].y, boostMenuPlayer1[1].width, boostMenuPlayer1[1].height, hdcMem, 0, 0, SRCPAINT);
		}
		if (player1.speed)
		{
			(HBITMAP)SelectObject(hdcMem, hbmBoostMenuPlayer1[2]);
			BitBlt(hdcBuffer, boostMenuPlayer1[2].x, boostMenuPlayer1[2].y, boostMenuPlayer1[2].width, boostMenuPlayer1[2].height, hdcMem, 0, 0, SRCPAINT);
		}
		if (player1.shield)
		{
			(HBITMAP)SelectObject(hdcMem, hbmBoostMenuPlayer1[3]);
			BitBlt(hdcBuffer, boostMenuPlayer1[3].x, boostMenuPlayer1[3].y, boostMenuPlayer1[3].width, boostMenuPlayer1[3].height, hdcMem, 0, 0, SRCAND);

			(HBITMAP)SelectObject(hdcMem, hbmBoostMenuMask[0]);
			BitBlt(hdcBuffer, boostMenuPlayer1[3].x, boostMenuPlayer1[3].y, boostMenuPlayer1[3].width, boostMenuPlayer1[3].height, hdcMem, 0, 0, SRCPAINT);

			(HBITMAP)SelectObject(hdcMem, hbmShieldPlayer1Mask);
			BitBlt(hdcBuffer, spaceship[Player1ship].x - (shieldOnPlayer1.width - spaceship[Player1ship].width)/2, spaceship[Player1ship].y - (shieldOnPlayer1.height - spaceship[Player1ship].height) / 2, shieldOnPlayer1.width, shieldOnPlayer1.height, hdcMem, 0, 0, SRCAND);

			(HBITMAP)SelectObject(hdcMem, hbmShieldPlayer1);
			BitBlt(hdcBuffer, spaceship[Player1ship].x - (shieldOnPlayer1.width - spaceship[Player1ship].width) / 2, spaceship[Player1ship].y - (shieldOnPlayer1.height - spaceship[Player1ship].height) / 2, shieldOnPlayer1.width, shieldOnPlayer1.height, hdcMem, 0, 0, SRCPAINT);
		}
		if (player1.health)
		{
			(HBITMAP)SelectObject(hdcMem, hbmBoostMenuPlayer1[4]);
			BitBlt(hdcBuffer, boostMenuPlayer1[4].x, boostMenuPlayer1[4].y, boostMenuPlayer1[4].width, boostMenuPlayer1[4].height, hdcMem, 0, 0, SRCPAINT);
		}
		if (glPlayers == 2)
		{

			if (player2.attack)
			{
				(HBITMAP)SelectObject(hdcMem, hbmBoostMenuPlayer2[0]);
				BitBlt(hdcBuffer, boostMenuPlayer2[0].x, boostMenuPlayer2[0].y, boostMenuPlayer2[0].width, boostMenuPlayer2[0].height, hdcMem, 0, 0, SRCPAINT);
			}
			if (player2.defense)
			{
				(HBITMAP)SelectObject(hdcMem, hbmBoostMenuPlayer2[1]);
				BitBlt(hdcBuffer, boostMenuPlayer2[1].x, boostMenuPlayer2[1].y, boostMenuPlayer2[1].width, boostMenuPlayer2[1].height, hdcMem, 0, 0, SRCPAINT);
			}
			if (player2.speed)
			{
				(HBITMAP)SelectObject(hdcMem, hbmBoostMenuPlayer2[2]);
				BitBlt(hdcBuffer, boostMenuPlayer2[2].x, boostMenuPlayer2[2].y, boostMenuPlayer2[2].width, boostMenuPlayer2[2].height, hdcMem, 0, 0, SRCPAINT);
			}
			if (player2.shield)
			{
				(HBITMAP)SelectObject(hdcMem, hbmBoostMenuPlayer2[3]);
				BitBlt(hdcBuffer, boostMenuPlayer2[3].x, boostMenuPlayer2[3].y, boostMenuPlayer2[3].width, boostMenuPlayer2[3].height, hdcMem, 0, 0, SRCAND);

				(HBITMAP)SelectObject(hdcMem, hbmBoostMenuMask[1]);
				BitBlt(hdcBuffer, boostMenuPlayer2[3].x, boostMenuPlayer2[3].y, boostMenuPlayer2[3].width, boostMenuPlayer2[3].height, hdcMem, 0, 0, SRCPAINT);

				(HBITMAP)SelectObject(hdcMem, hbmShieldPlayer2Mask);
				BitBlt(hdcBuffer, spaceship[Player2ship].x - (shieldOnPlayer2.width - spaceship[Player2ship].width) / 2, spaceship[Player2ship].y - (shieldOnPlayer2.height - spaceship[Player2ship].height) / 2, shieldOnPlayer2.width, shieldOnPlayer2.height, hdcMem, 0, 0, SRCAND);

				(HBITMAP)SelectObject(hdcMem, hbmShieldPlayer2);
				BitBlt(hdcBuffer, spaceship[Player2ship].x - (shieldOnPlayer2.width - spaceship[Player2ship].width) / 2, spaceship[Player2ship].y - (shieldOnPlayer2.height - spaceship[Player2ship].height) / 2, shieldOnPlayer2.width, shieldOnPlayer2.height, hdcMem, 0, 0, SRCPAINT);
				
			}
			if (player2.health)
			{
				(HBITMAP)SelectObject(hdcMem, hbmBoostMenuPlayer2[4]);
				BitBlt(hdcBuffer, boostMenuPlayer2[4].x, boostMenuPlayer2[4].y, boostMenuPlayer2[4].width, boostMenuPlayer2[4].height, hdcMem, 0, 0, SRCPAINT);
			}
		}

		if (GetTickCount() - boostTimer > 10000)
		{
			boostOnScreen = true;
			boostRandomizer = rand() % 5;
			boostTimer = GetTickCount();
		}
		if (boostOnScreen)
		{
			if (boostRandomizer == 3)
			{
				(HBITMAP)SelectObject(hdcMem, hbmBoostMask);
				BitBlt(hdcBuffer, boostObj[boostRandomizer].x, boostObj[boostRandomizer].y, boostObj[boostRandomizer].width, boostObj[boostRandomizer].height, hdcMem, 0, 0, SRCPAINT);
				(HBITMAP)SelectObject(hdcMem, hbmBoost[boostRandomizer]);
				BitBlt(hdcBuffer, boostObj[boostRandomizer].x, boostObj[boostRandomizer].y, boostObj[boostRandomizer].width, boostObj[boostRandomizer].height, hdcMem, 0, 0, SRCAND);
			}
			else {
				(HBITMAP)SelectObject(hdcMem, hbmBoost[boostRandomizer]);
				BitBlt(hdcBuffer, boostObj[boostRandomizer].x, boostObj[boostRandomizer].y, boostObj[boostRandomizer].width, boostObj[boostRandomizer].height, hdcMem, 0, 0, SRCPAINT);
			}
		}

		(HBITMAP)SelectObject(hdcMem, hbmSpaceshipMask[Player1ship]);
		BitBlt(hdcBuffer, spaceship[Player1ship].x, spaceship[Player1ship].y, spaceship[Player1ship].width, spaceship[Player1ship].height, hdcMem, spaceshipX*spaceship[Player1ship].width, 0, SRCAND);

		hbmOld1 = (HBITMAP)SelectObject(hdcMem, hbmSpaceship[Player1ship]);
		BitBlt(hdcBuffer, spaceship[Player1ship].x, spaceship[Player1ship].y, spaceship[Player1ship].width, spaceship[Player1ship].height, hdcMem, spaceshipX*spaceship[Player1ship].width, 0, SRCPAINT);

		if (glPlayers == 2)
		{
			(HBITMAP)SelectObject(hdcMem, hbmSpaceshipMask[Player2ship]);
			BitBlt(hdcBuffer, spaceship[Player2ship].x, spaceship[Player2ship].y, spaceship[Player2ship].width, spaceship[Player2ship].height, hdcMem, spaceship2X*spaceship[Player2ship].width, 0, SRCAND);

			HBITMAP hbmOld1 = (HBITMAP)SelectObject(hdcMem, hbmSpaceship[Player2ship]);
			BitBlt(hdcBuffer, spaceship[Player2ship].x, spaceship[Player2ship].y, spaceship[Player2ship].width, spaceship[Player2ship].height, hdcMem, spaceship2X*spaceship[Player2ship].width, 0, SRCPAINT);
		}

		for (Object stone : stonesSteelR)
		{
			(HBITMAP)SelectObject(hdcMem, hbmStonesSteelMask[stoneSteelCounterRight]);
			BitBlt(hdcBuffer, stone.x, stone.y, stone.width, stone.height, hdcMem, stones1X*stonesSteel[stoneSteelCounterRight].width, stones1Y*stonesSteel[stoneSteelCounterRight].height, SRCAND);

			hbmOld = (HBITMAP)SelectObject(hdcMem, hbmStonesSteel[stoneSteelCounterRight]);
			BitBlt(hdcBuffer, stone.x, stone.y, stone.width, stone.height, hdcMem, stones1X*stonesSteel[stoneSteelCounterRight].width, stones1Y*stonesSteel[stoneSteelCounterRight].height, SRCPAINT);
			stoneSteelCounterRight++;
		}
		for (Object stone : stonesSteelL)
		{
			(HBITMAP)SelectObject(hdcMem, hbmStonesSteelMask[stoneSteelCounterLeft]);
			BitBlt(hdcBuffer, stone.x, stone.y, stone.width, stone.height, hdcMem, stones3X*stonesSteel[stoneSteelCounterLeft].width, stones3Y*stonesSteel[stoneSteelCounterLeft].height, SRCAND);

			hbmOld = (HBITMAP)SelectObject(hdcMem, hbmStonesSteel[stoneSteelCounterLeft]);
			BitBlt(hdcBuffer, stone.x, stone.y, stone.width, stone.height, hdcMem, stones3X*stonesSteel[stoneSteelCounterLeft].width, stones3Y*stonesSteel[stoneSteelCounterLeft].height, SRCPAINT);
			stoneSteelCounterLeft++;
		}

		for (Object stone : stonesGroundL)
		{
			(HBITMAP)SelectObject(hdcMem, hbmStonesGroundMask[stoneGroundCounterLeft]);
			BitBlt(hdcBuffer, stone.x, stone.y, stone.width, stone.height, hdcMem, stones4X*stonesGround[stoneGroundCounterLeft].width, stones4Y*stonesGround[stoneGroundCounterLeft].height, SRCAND);

			hbmOld = (HBITMAP)SelectObject(hdcMem, hbmStonesGround[stoneGroundCounterLeft]);
			BitBlt(hdcBuffer, stone.x, stone.y, stone.width, stone.height, hdcMem, stones4X*stonesGround[stoneGroundCounterLeft].width, stones4Y*stonesGround[stoneGroundCounterLeft].height, SRCPAINT);
			stoneGroundCounterLeft++;
		}

		for (Object stone : stonesGroundR)
		{
			(HBITMAP)SelectObject(hdcMem, hbmStonesGroundMask[stoneGroundCounterRight]);
			BitBlt(hdcBuffer, stone.x, stone.y, stone.width, stone.height, hdcMem, stones2X*stonesGround[stoneGroundCounterRight].width, stones2Y*stonesGround[stoneGroundCounterRight].height, SRCAND);

			hbmOld = (HBITMAP)SelectObject(hdcMem, hbmStonesGround[stoneGroundCounterLeft]);
			BitBlt(hdcBuffer, stone.x, stone.y, stone.width, stone.height, hdcMem, stones2X*stonesGround[stoneGroundCounterRight].width, stones2Y*stonesGround[stoneGroundCounterRight].height, SRCPAINT);
			stoneGroundCounterRight++;
		}


		if (player1.attack) {
			while (bulletsOnScreen3 > bulletsBoost.size())
			{
				bulletsBoost.push_back(bulletBoost[bulletCounter3]);
				bulletCounter3++;
			}

			int z = 0;
			for (Object bulletBoost : bulletsBoost)
			{

				(HBITMAP)SelectObject(hdcMem, hbmBulletBoostMask[z]);
				BitBlt(hdcBuffer, bulletBoost.x, bulletBoost.y, bulletBoost.width, bulletBoost.height, hdcMem, 0, 0, SRCAND);

				hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBulletBoost[z]);
				BitBlt(hdcBuffer, bulletBoost.x, bulletBoost.y, bulletBoost.width, bulletBoost.height, hdcMem, 0, 0, SRCPAINT);

				z++;
			}
		}
		else {
			while (bulletsOnScreen > bullets.size())
			{
				bullets.push_back(bullet[bulletCounter]);
				bulletCounter++;
			}
			int i = 0;
			for (Object bullet : bullets)
			{

				(HBITMAP)SelectObject(hdcMem, hbmBulletMask[i]);
				BitBlt(hdcBuffer, bullet.x, bullet.y, bullet.width, bullet.height, hdcMem, 0, 0, SRCAND);

				hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBullet[i]);
				BitBlt(hdcBuffer, bullet.x, bullet.y, bullet.width, bullet.height, hdcMem, 0, 0, SRCPAINT);

				i++;
			}
		}

		while (bulletsOnScreen2 > bulletsMagnum.size())
		{
			bulletsMagnum.push_back(bulletMagnum[bulletCounter2]);
			bulletCounter2++;
		}
		int w = 0;
		for (Object bulletMagnum : bulletsMagnum)
		{

			(HBITMAP)SelectObject(hdcMem, hbmBulletMagnumMask[w]);
			BitBlt(hdcBuffer, bulletMagnum.x, bulletMagnum.y, bulletMagnum.width, bulletMagnum.height, hdcMem, 0, 0, SRCAND);

			hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBulletMagnum[w]);
			BitBlt(hdcBuffer, bulletMagnum.x, bulletMagnum.y, bulletMagnum.width, bulletMagnum.height, hdcMem, 0, 0, SRCPAINT);

			w++;
		}

		if (collisionHappened)
		{
			(HBITMAP)SelectObject(hdcMem, hbmExplosionMask[0]);
			BitBlt(hdcBuffer, explosion[0].x, explosion[0].y, explosion[0].width, explosion[0].height, hdcMem, explosionX*explosion[0].width, explosionY*explosion[0].height, SRCAND);

			HBITMAP hbmOld1 = (HBITMAP)SelectObject(hdcMem, hbmExplosion[0]);
			BitBlt(hdcBuffer, explosion[0].x, explosion[0].y, explosion[0].width, explosion[0].height, hdcMem, explosionX*explosion[0].width, explosionY*explosion[0].height, SRCPAINT);

		}

		(HBITMAP)SelectObject(hdcMem, hbmHealthbarLMask);
		BitBlt(hdcBuffer, healthbarL.x, healthbarL.y, healthbarL.width, healthbarL.height, hdcMem, 0, 0, SRCAND);

		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmHealthbarL);
		BitBlt(hdcBuffer, healthbarL.x, healthbarL.y, healthbarL.width, healthbarL.height, hdcMem, 0, 0, SRCPAINT);


		(HBITMAP)SelectObject(hdcMem, hbmHealthbarRMask);
		BitBlt(hdcBuffer, healthbarR.x, healthbarR.y, healthbarR.width, healthbarR.height, hdcMem, 0, 0, SRCAND);

		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmHealthbarL);
		BitBlt(hdcBuffer, healthbarR.x, healthbarR.y, healthbarR.width, healthbarR.height, hdcMem, 0, 0, SRCPAINT);

		for (int greenNum = 0; greenNum < player1.numOfGreenSquares; greenNum++)
		{
			(HBITMAP)SelectObject(hdcMem, hbmGreen);
			BitBlt(hdcBuffer, green[greenNum].x, green[greenNum].y, green[greenNum].width, green[greenNum].height, hdcMem, 0, 0, SRCAND);

			hbmOld = (HBITMAP)SelectObject(hdcMem, hbmGreen);
			BitBlt(hdcBuffer, green[greenNum].x, green[greenNum].y, green[greenNum].width, green[greenNum].height, hdcMem, 0, 0, SRCPAINT);
		}


		for (int greenNum = 0; greenNum < player2.numOfGreenSquares; greenNum++)
		{
			(HBITMAP)SelectObject(hdcMem, hbmGreen);
			BitBlt(hdcBuffer, green2[greenNum].x, green2[greenNum].y, green2[greenNum].width, green2[greenNum].height, hdcMem, 0, 0, SRCAND);

			hbmOld = (HBITMAP)SelectObject(hdcMem, hbmGreen);
			BitBlt(hdcBuffer, green2[greenNum].x, green2[greenNum].y, green2[greenNum].width, green2[greenNum].height, hdcMem, 0, 0, SRCPAINT);
		}

		(HBITMAP)SelectObject(hdcMem, hbmBossMask[0]);
		BitBlt(hdcBuffer, boss[0].x, boss[0].y, boss[0].width, boss[0].height, hdcMem, 0, 0, SRCAND);

		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBoss[0]);
		BitBlt(hdcBuffer, boss[0].x, boss[0].y, boss[0].width, boss[0].height, hdcMem, 0, 0, SRCPAINT);

	}


	BitBlt(hdc, 0, 0, prect->right, prect->bottom, hdcBuffer, 0, 0, SRCCOPY);

	spaceshipX++;
	spaceship2X++;
	stones1X++;
	if (stones1X == 8)
	{
		stones1Y++;
		stones1X = 0;
		if (stones1Y == 4)
			stones1Y = 0;
	}
	stones2X++;
	if (stones2X == 8)
	{
		stones2Y++;
		stones2X = 0;
		if (stones2Y == 4)
			stones2Y = 0;
	}
	stones3X++;
	if (stones3X == 8)
	{
		stones3Y++;
		stones3X = 0;
		if (stones3Y == 8)
			stones3Y = 4;
	}
	stones4X++;
	if (stones4X == 8)
	{
		stones4Y++;
		stones4X = 0;
		if (stones4Y == 8)
			stones4Y = 4;
	}

	if (Player1ship == 0 || Player1ship == 2)
	{
		if (spaceshipX == 7)
			spaceshipX = 0;
	}
	else if (Player1ship == 1 || Player1ship == 4 || Player1ship == 7 || Player1ship == 9)
	{
		if (spaceshipX == 4)
			spaceshipX = 0;
	}
	else if (Player1ship == 3 || Player1ship == 6)
	{
		if (spaceshipX == 6)
			spaceshipX = 0;
	}
	else if (Player1ship == 5 || Player1ship == 8)
	{
		if (spaceshipX == 8)
			spaceshipX = 0;
	}
	if (Player2ship == 0 || Player2ship == 2)
	{
		if (spaceship2X == 7)
			spaceship2X = 0;
	}
	else if (Player2ship == 1 || Player2ship == 4 || Player2ship == 7 || Player2ship == 9)
	{
		if (spaceship2X == 4)
			spaceship2X = 0;
	}
	else if (Player2ship == 3 || Player2ship == 6)
	{
		if (spaceship2X == 6)
			spaceship2X = 0;
	}
	else if (Player2ship == 5 || Player2ship == 8)
	{
		if (spaceship2X == 8)
			spaceship2X = 0;
	}

	explosionX++;
	if (explosionX == 4)
	{
		explosionX = 0;
		explosionY++;
		if (explosionY == 4)
		{
			collisionHappened = false;
			explosionY = 0;
		}
	}


	SelectObject(hdcMem, hbmOld);

	DeleteObject(hdcMem);
	DeleteObject(hbmBuffer);

	SelectObject(hdcBuffer, hbmOldBuffer);
	DeleteObject(hdcBuffer);
	DeleteObject(hbmOldBuffer);
	DeleteObject(hbmBuffer);
}