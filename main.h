#include <cstdlib>
#include <iostream>
#include <ctime>
#include <random>
#include <tchar.h>
#include <windows.h>
#include <MMSystem.h>
#include <vector>
#include <random>
#include <string>
#include <fstream>
#include <sstream>
using namespace std;

#define KEYDOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)

#define BGSTART         0;
#define BG1PHOVER       1;
#define BG2PHOVER       2;
#define BGCREDHOVER     3;
#define BGCTRLHOVER     4;
#define BGLOADHOVER     5;
#define BGEXITHOVER     6;
#define BGCTRL          7;
#define BGCRED          8;
#define BGPLAYHOVER     9;
#define BGPLAY          10;
#define BGSHIPHOVER     11;
#define BGSAVEHOVER     12;
#define BGMAINHOVER     13;
#define BGPAUSE         14;
#define BGPSHIPHOVER    15;
#define BGPSAVEHOVER    16;
#define BGPMAINHOVER    17;
#define BGMDZ           18;
#define BGCRR           19;
#define BGMGNM          20;
#define BGBUG           21;
#define BGSHARP         22;
#define BGEJ            23;
#define BGTUZLA         24;
#define BGFETM          25;
#define BGFETE          26;
#define BGMARVEL        27;
#define BGLVLCMP        28;
#define LEVEL1          29;
#define LEVEL2          30;
#define LEVEL3          31;
#define LEVEL4          32;
#define LEVEL5          33;
#define LEVEL6          34;
#define LEVEL7          35;
#define GAMEOVER        36;

int glBackGround = 0;
int glPlayers = 2;
int Player1ship = 6;
int Player2ship = 7;
int currentLevel = 0;

//  Declare Windows procedure
LRESULT CALLBACK WindowProcedure(HWND, UINT, WPARAM, LPARAM);

//  Make the class name into a global variable
TCHAR szClassName[] = _T("CodeBlocksWindowsApp");

LPCWSTR message = L"My First Game";
int hardness = 300;
int hardness2 = 300;
int stoneSpeed = 5;
bool k = true;
//CFmod g_Music;
DWORD pocvr=0;
DWORD pocvr2;
DWORD bulletCounter = 0;
DWORD bulletCounter2 = 0;
DWORD bulletCounter3 = 0;
DWORD stoneSteelCounterLeft = 0;
DWORD stoneSteelCounterRight = 0;
DWORD stoneGroundCounterLeft = 0;
DWORD stoneGroundCounterRight = 0;
DWORD boostTimer;
DWORD bossTimer;
bool exitMarker = true;
bool collisionHappened = false;
bool bossDead = false;
bool walkdown = true;
bool changedShip1 = false;
bool changedShip2 = false;
bool hasNotBeenSet1 = true;
bool hasNotBeenSet2 = true;
int counterBullet = 0;
int counterBullet2 = 0;
int bossBulletCounter = 0;
int shieldPlayer1 = 0;
int shieldPlayer2 = 0;

void CheckInput(HWND);
void Render(HWND);
void Draw(HDC, RECT*);
void UpdateGamePlay(HWND);
BOOL Initialize(void);
void bossAI(void);
 int PAUZA = 20;
int anumber = 5;
TCHAR score[];
int scoreNum = 50;
int spaceshipX = 0;
int spaceship2X = 0;
int stones1X = 0;
int stones1Y = 0;
int stones2X = 0;
int stones2Y = 0;
int stones3X = 0;
int stones3Y = 4;
int stones4X = 0;
int stones4Y = 4;
int explosionX = 0;
int explosionY = 0;
bool boostOnScreen = false;
bool markerForGetTickCountSR = true;
bool markerForGetTickCountSL = true;
bool markerForGetTickCountGR = true;
bool markerForGetTickCountGL = true;
DWORD timeSinceStoneSR;
DWORD timeSinceStoneSL;
DWORD timeSinceStoneGL;
DWORD timeSinceStoneGR;
int boostRandomizer = 0;
string line;
ifstream load("save.txt");
ofstream save;
typedef struct Objects
{
	int height;
	int width;
	int x;
	int y;
	int dx;
	int dy;
} Object;

struct Level
{
	int lowerBoundTime;
	int upperBoundTime;
	int lowerBoundSpeed;
	int upperBoundSpeed;
	int timeDuration;
} ;
Level levels[7];
struct Player : Object
{

	int numOfGreenSquares;
	int def;
	bool speed;
	bool shield;
	bool attack;
	bool defense;
	bool health;

};
int bulletsOnScreen = 0;
int bulletsOnScreen2 = 0;
int bulletsOnScreen3 = 0;
int bossBulletsOnScreen = 0;

vector<Object> bossBullets;
vector<Object> bullets;
vector<Object> bulletsMagnum;
vector<Object> bulletsBoost;
vector<Object> stonesSteelR,stonesSteelL,stonesGroundL,stonesGroundR;
vector<Object> explosions;

HBITMAP hbmBossBullet[10], hbmBossBulletMask[10], hbmBoostMask, hbmBoostMenuMask[2];
HBITMAP hbmBullet[10], hbmBulletMask[10], hbmBulletBoost[10], hbmBulletBoostMask[10], hbmBulletMagnum[10], hbmBulletMagnumMask[10];
Object  background, bullet[30], bulletBoost[10], spaceship[10], boostObj[5], stonesSteel[50], stonesGround[50], bulletMagnum[10];
Object  explosion[10], bossBullet[10], shieldOnPlayer1, shieldOnPlayer2;

HBITMAP hbmBackground[37],hbmBoostMenuPlayer1[5],hbmBoostMenuPlayer2[5];
HBITMAP hbmItemsPlayer1, hbmItemsPlayer1Mask, hbmItemsPlayer2, hbmItemsPlayer2Mask;
HBITMAP hbmBoss[10], hbmBossMask[10],hbmSpaceship[10], hbmSpaceshipMask[10], hbmStonesSteel[50], hbmStonesSteelMask[50], hbmStonesGround[50], hbmStonesGroundMask[50],hbmBoost[5];
HBITMAP hbmBoostMenu[5], hbmGreen, hbmItems, hbmItemsMask, hbmScore;
HBITMAP hbmHealthbarL, hbmHealthbarR, hbmHealthbarLMask, hbmHealthbarRMask;
HBITMAP hbmFog, hbmFogMask, hbmExplosion[10], hbmExplosionMask[10];
Object fog,healthbarL,healthbarR, green[30], green2[30], itemsPlayer2,itemsPlayer1, boostMenuPlayer1[5], boostMenuPlayer2[5];
Player player1, player2, boss[10];
HBITMAP hbmShieldPlayer1, hbmShieldPlayer1Mask, hbmShieldPlayer2, hbmShieldPlayer2Mask;
int speedPlayer1 = 0, speedPlayer2 = 0;
DWORD bulletCounterPlayer2 = 0;
DWORD bulletCounter2Player2 = 0;
int counterBulletPlayer2 = 0;
int counterBullet2Player2 = 0;
int bulletsOnScreenPlayer2 = 0;
int bulletsOnScreen2Player2 = 0;
vector<Object> bulletsPlayer2;
vector<Object> bulletsBoostPlayer2;
HBITMAP hbmBulletBoostPlayer2[10], hbmBulletBoostMaskPlayer2[10], hbmBulletPlayer2[10], hbmBulletMaskPlayer2[10];
Object bulletPlayer2[10], bulletBoostPlayer2[10];