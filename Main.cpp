﻿#if defined(UNICODE) && !defined(_UNICODE)
#define _UNICODE
#elif defined(_UNICODE) && !defined(UNICODE)
#define UNICODE
#endif
#pragma comment(lib, "Winmm.lib")				// Include this windows library for PlaySound()
#include <thread>

#include"main.h"
#include <WinUser.h>



bool checkIfSafe(vector<Object> vec)
{
	bool returnValue = true;
	for (int position = boss[0].x; position < boss[0].x + 500; position++)
	{
		for (Object bullet : vec)
		{
			if (position == bullet.x)
			{
				boss[0].x -= boss[0].dx;
				returnValue = false;
			}
		}
	}
	for (int position2 = boss[0].x; position2 > boss[0].x - 500; position2--)
	{
		for (Object bullet : vec)
		{
			if (position2 == bullet.x)
			{
				boss[0].x += boss[0].dx;
				returnValue = false;
			}
		}
	}
	return returnValue;
}
void bossAI(void)
{
	

	if (abs(spaceship[Player1ship].x - boss[0].x) < abs(spaceship[Player2ship].x - boss[0].x))
	{

		if (spaceship[Player1ship].x > boss[0].x)
		{
			if (checkIfSafe(bullets) && checkIfSafe(bulletsMagnum) && checkIfSafe(bulletsBoost) && checkIfSafe(bulletsPlayer2) && checkIfSafe(bulletsBoostPlayer2))
				boss[0].x += boss[0].dx;
		}
		else if (spaceship[Player1ship].x < boss[0].x)
		{
			if (checkIfSafe(bullets) && checkIfSafe(bulletsMagnum) && checkIfSafe(bulletsBoost) && checkIfSafe(bulletsPlayer2) && checkIfSafe(bulletsBoostPlayer2))
				boss[0].x -= boss[0].dx;
		}
	}
	else
	{
		if (spaceship[Player2ship].x > boss[0].x)
		{
			if (checkIfSafe(bullets) && checkIfSafe(bulletsMagnum) && checkIfSafe(bulletsBoost) && checkIfSafe(bulletsPlayer2) && checkIfSafe(bulletsBoostPlayer2))
				if (boss[0].x > 1850)
					boss[0].x -= boss[0].dx;
				else
					boss[0].x += boss[0].dx;
		}
		else if (spaceship[Player2ship].x < boss[0].x)
		{
			if (checkIfSafe(bullets) && checkIfSafe(bulletsMagnum) && checkIfSafe(bulletsBoost) && checkIfSafe(bulletsPlayer2) && checkIfSafe(bulletsBoostPlayer2))
				if (boss[0].x < 300)
					boss[0].x += boss[0].dx;
				else
					boss[0].x -= boss[0].dx;
		}
	}

	if (walkdown)
	{
		if (boss[0].y<150)
			boss[0].y += boss[0].dy;
		else
			walkdown = false;
	}
	else {
		boss[0].y -= boss[0].dy;
		if (boss[0].y < 20)
			walkdown = true;
	}
	if (boss[0].x < -boss[0].width)
		boss[0].x = 1920;
	else if (boss[0].x > 1920)
		boss[0].x = -boss[0].width;
}

bool collision(Object rect1, Object rect2)
{
	if (rect1.x < rect2.x + rect2.width &&
		rect1.x + rect1.width > rect2.x &&
		rect1.y < rect2.y + rect2.height &&
		rect1.y + rect1.height > rect2.y)
	{
		collisionHappened = true;
		return true;
	}
	else return false;
}

bool boostCollision(Object rect1, Object rect2)
{
	if (rect1.x < rect2.x + rect2.width &&
		rect1.x + rect1.width > rect2.x &&
		rect1.y < rect2.y + rect2.height &&
		rect1.y + rect1.height > rect2.y)
		return true;
	else 
		return false;
}

void checkForCollisions()
{
	vector<Object>::iterator it = bullets.begin();
	for (Object collider : bullets)
	{
		if (collision(boss[0], collider))
		{
			explosion[0].x = boss[0].x - 100;
			explosion[0].y = boss[0].y;
			boss[0].numOfGreenSquares--;
			bullets.erase(it);
			bulletsOnScreen--;
			
			if (boss[0].numOfGreenSquares <= 0)
			{
				boss[0].y = 10000;
				bossDead = true;
			}

		}
		else
		it++;
	}
	it = bulletsPlayer2.begin();
	for (Object collider : bulletsPlayer2)
	{
		if (collision(boss[0], collider))
		{
			explosion[0].x = boss[0].x - 100;
			explosion[0].y = boss[0].y;
			boss[0].numOfGreenSquares--;
			bulletsPlayer2.erase(it);
			bulletsOnScreenPlayer2--;

			if (boss[0].numOfGreenSquares <= 0)
			{
				boss[0].y = 10000;
				bossDead = true;
			}

		}
		else
			it++;
	}
	it = bulletsMagnum.begin();
	for (Object collider : bulletsMagnum)
	{
		if (collision(boss[0], collider))
		{
			explosion[0].x = boss[0].x - 100;
			explosion[0].y = boss[0].y;
			
			if ((player1.attack && Player1ship == 2) || (player2.attack && Player2ship == 2))
					boss[0].numOfGreenSquares -= 3;
			else
			boss[0].numOfGreenSquares -= 2;

			bulletsMagnum.erase(it);
			bulletsOnScreen2--;
			
			if (boss[0].numOfGreenSquares <= 0)
			{
				boss[0].y = 10000;
				bossDead = true;
			}
		}
		else
		it++;
	}
	it = bulletsBoost.begin();
	for (Object collider : bulletsBoost)
	{
		if (collision(boss[0], collider))
		{
			explosion[0].x = boss[0].x - 100;
			explosion[0].y = boss[0].y;
			boss[0].numOfGreenSquares -= 2;
			bulletsBoost.erase(it);
			bulletsOnScreen3--;
			if (boss[0].numOfGreenSquares <= 0)
			{
				boss[0].y = 10000;
				bossDead = true;
			}
		}
		else
		it++;
	}
	it = bulletsBoostPlayer2.begin();
	for (Object collider : bulletsBoostPlayer2)
	{
		if (collision(boss[0], collider))
		{
			explosion[0].x = boss[0].x - 100;
			explosion[0].y = boss[0].y;
			boss[0].numOfGreenSquares -= 2;
			bulletsBoostPlayer2.erase(it);
			bulletsOnScreen2Player2--;
			if (boss[0].numOfGreenSquares <= 0)
			{
				boss[0].y = 10000;
				bossDead = true;
			}
		}
		else
			it++;
	}
	it = stonesSteelR.begin();
	for (Object collider : stonesSteelR)
	{
		if (collision(spaceship[Player1ship], collider))
		{
			explosion[0].x = spaceship[Player1ship].x - 90;
			explosion[0].y = spaceship[Player1ship].y - 80;
			stonesSteelR.erase(it);
			switch (Player1ship)
			{
			case 0:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 1:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 4;
				}
				break;
			case 2:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 2;
				}
				break;
			case 3:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 2;
				}
				break;
			case 4:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 6;
				}
				break;
			case 5:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 6:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 7:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 2;
				}
				break;
			case 8:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 9:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			}
		}
		else
		it++;
	}
	it = stonesSteelL.begin();
	for (Object collider : stonesSteelL)
	{
		if (collision(spaceship[Player1ship], collider))
		{
			explosion[0].x = spaceship[Player1ship].x - 90;
			explosion[0].y = spaceship[Player1ship].y - 80;
			stonesSteelL.erase(it);
			switch (Player1ship)
			{
			case 0:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 1:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 4;
				}
				break;
			case 2:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 2;
				}
				break;
			case 3:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 2;
				}
				break;
			case 4:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 6;
				}
				break;
			case 5:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 6:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 7:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 2;
				}
				break;
			case 8:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 9:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			}
		}
		else
		it++;
	}
	it = stonesGroundL.begin();
	for (Object collider : stonesGroundL)
	{

		if (collision(spaceship[Player1ship], collider))
		{
			explosion[0].x = spaceship[Player1ship].x - 90;
			explosion[0].y = spaceship[Player1ship].y - 80;
			stonesGroundL.erase(it);
			switch (Player1ship)
			{
			case 0:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 1:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 4;
				}
				break;
			case 2:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 2;
				}
				break;
			case 3:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 2;
				}
				break;
			case 4:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 6;
				}
				break;
			case 5:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 6:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 7:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 2;
				}
				break;
			case 8:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 9:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			}
		}
		else
		it++;
	}
	it = stonesGroundR.begin();
	for (Object collider : stonesGroundR)
	{
		if (collision(spaceship[Player1ship], collider))
		{
			explosion[0].x = spaceship[Player1ship].x - 90;
			explosion[0].y = spaceship[Player1ship].y - 80;
			stonesGroundR.erase(it);
			switch (Player1ship)
			{
			case 0:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 1:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 4;
				}
				break;
			case 2:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 2;
				}
				break;
			case 3:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 2;
				}
				break;
			case 4:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 6;
				}
				break;
			case 5:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 6:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 7:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 2;
				}
				break;
			case 8:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 9:
				player1.def--;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			}
		}
		else
		it++;

	}
	it = stonesSteelR.begin();
	for (Object collider : stonesSteelR)
	{
		if (collision(spaceship[Player2ship], collider))
		{
			explosion[0].x = spaceship[Player2ship].x - 90;
			explosion[0].y = spaceship[Player2ship].y - 80;
			stonesSteelR.erase(it);
			switch (Player2ship)
			{
			case 0:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 1:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 4;
				}
				break;
			case 2:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 2;
				}
				break;
			case 3:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 2;
				}
				break;
			case 4:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 6;
				}
				break;
			case 5:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 6:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 7:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 2;
				}
				break;
			case 8:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 9:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			}
		}
		else
		it++;
	}
	it = stonesSteelL.begin();
	for (Object collider : stonesSteelL)
	{
		if (collision(spaceship[Player2ship], collider))
		{
			explosion[0].x = spaceship[Player2ship].x - 90;
			explosion[0].y = spaceship[Player2ship].y - 80;
			stonesSteelL.erase(it);
			switch (Player2ship)
			{
			case 0:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 1:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 4;
				}
				break;
			case 2:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 2;
				}
				break;
			case 3:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 2;
				}
				break;
			case 4:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 6;
				}
				break;
			case 5:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 6:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 7:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 2;
				}
				break;
			case 8:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 9:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			}
		}
		else
		it++;
	}
	it = stonesGroundL.begin();
	for (Object collider : stonesGroundL)
	{
		if (collision(spaceship[Player2ship], collider))
		{
			explosion[0].x = spaceship[Player2ship].x - 90;
			explosion[0].y = spaceship[Player2ship].y - 80;
			stonesGroundL.erase(it);
			switch (Player2ship)
			{
			case 0:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 1:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 4;
				}
				break;
			case 2:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 2;
				}
				break;
			case 3:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 2;
				}
				break;
			case 4:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 6;
				}
				break;
			case 5:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 6:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 7:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 2;
				}
				break;
			case 8:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 9:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			}

		}
		else
		it++;
	}
	it = stonesGroundR.begin();
	for (Object collider : stonesGroundR)
	{
		if (collision(spaceship[Player2ship], collider))
		{
			explosion[0].x = spaceship[Player2ship].x - 90;
			explosion[0].y = spaceship[Player2ship].y - 80;
			stonesGroundR.erase(it);
			switch (Player2ship)
			{
			case 0:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 1:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 4;
				}
				break;
			case 2:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 2;
				}
				break;
			case 3:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 2;
				}
				break;
			case 4:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 6;
				}
				break;
			case 5:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 6:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 7:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 2;
				}
				break;
			case 8:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 9:
				player2.def--;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			}
		}
		else
		it++;
	}

	it = bossBullets.begin();
	for (Object collider : bossBullets)
	{
		if (collision(shieldOnPlayer1, collider))
		{
			explosion[0].x = collider.x - explosion[0].width/2 + collider.width/2;
			explosion[0].y = collider.y - explosion[0].height/2;
			bossBullets.erase(it);
			bossBulletsOnScreen--;
			shieldPlayer1--;
		}
		else
			it++;

	}

	it = bossBullets.begin();
	for (Object collider : bossBullets)
	{
		if (collision(shieldOnPlayer2, collider))
		{
			explosion[0].x = collider.x - explosion[0].width/2 + collider.width / 2;
			explosion[0].y = collider.y - explosion[0].height / 2;
			bossBullets.erase(it);
			bossBulletsOnScreen--;
			shieldPlayer2--;
		}
		else
			it++;

	}

	it = bossBullets.begin();
	for (Object collider : bossBullets)
	{
		if (collision(spaceship[Player1ship], collider))
		{
			explosion[0].x = spaceship[Player1ship].x - 90;
			explosion[0].y = spaceship[Player1ship].y - 80;
			bossBullets.erase(it);
			bossBulletsOnScreen--;
			switch (Player1ship)
			{
			case 0:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 1:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 4;
				}
				break;
			case 2:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 2;
				}
				break;
			case 3:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 2;
				}
				break;
			case 4:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 6;
				}
				break;
			case 5:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 6:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 7:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 2;
				}
				break;
			case 8:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			case 9:
				player1.def -= 2;
				if (player1.def <= 0)
				{
					player1.numOfGreenSquares--;
					player1.def = 3;
				}
				break;
			}
		}
		else
		it++;
	}

	it = bossBullets.begin();
	for (Object collider : bossBullets)
	{
		if (collision(spaceship[Player2ship], collider))
		{
			explosion[0].x = spaceship[Player2ship].x - 90;
			explosion[0].y = spaceship[Player2ship].y - 80;
			bossBullets.erase(it);
			bossBulletsOnScreen--;
			switch (Player2ship)
			{
			case 0:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 1:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 4;
				}
				break;
			case 2:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 2;
				}
				break;
			case 3:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 2;
				}
				break;
			case 4:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 6;
				}
				break;
			case 5:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 6:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 7:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 2;
				}
				break;
			case 8:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			case 9:
				player2.def -= 2;
				if (player2.def <= 0)
				{
					player2.numOfGreenSquares--;
					player2.def = 3;
				}
				break;
			}
		}
		else
		it++;
	}

	vector<Object>::iterator itBullet = bullets.begin();
	for (Object collider1 : bullets)
	{
		k = true;
		vector<Object>::iterator itStoneSteelL = stonesSteelL.begin();
		for (Object collider2 : stonesSteelL)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesSteelL.erase(itStoneSteelL);
				bullets.erase(itBullet);
				bulletsOnScreen--;
				k = false;
			}
			else
			itStoneSteelL++;
		}
		vector<Object>::iterator itStoneSteelR = stonesSteelR.begin();
		for (Object collider2 : stonesSteelR)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesSteelR.erase(itStoneSteelR);
				bullets.erase(itBullet);
				bulletsOnScreen--;
				k = false;
			}
			else
			itStoneSteelR++;
		}
		vector<Object>::iterator itStoneGroundL = stonesGroundL.begin();
		for (Object collider2 : stonesGroundL)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesGroundL.erase(itStoneGroundL);
				bullets.erase(itBullet);
				bulletsOnScreen--;
				k = false;
			}
			else
			itStoneGroundL++;
		}
		vector<Object>::iterator itStoneGroundR = stonesGroundR.begin();
		for (Object collider2 : stonesGroundR)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesGroundR.erase(itStoneGroundR);
				bullets.erase(itBullet);
				bulletsOnScreen--;
				k = false;
			}
			else
			itStoneGroundR++;
		}
		if (k)
		itBullet++;
	}

	itBullet = bulletsMagnum.begin();
	for (Object collider1 : bulletsMagnum)
	{
		k = true;
		vector<Object>::iterator itStoneSteelL = stonesSteelL.begin();
		for (Object collider2 : stonesSteelL)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesSteelL.erase(itStoneSteelL);
				bulletsMagnum.erase(itBullet);
				bulletsOnScreen2--;
				k = false;
			}
			else
			itStoneSteelL++;
		}
		vector<Object>::iterator itStoneSteelR = stonesSteelR.begin();
		for (Object collider2 : stonesSteelR)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesSteelR.erase(itStoneSteelR);
				bulletsMagnum.erase(itBullet);
				bulletsOnScreen2--;
				k = false;
			}
			else
			itStoneSteelR++;
		}
		vector<Object>::iterator itStoneGroundL = stonesGroundL.begin();
		for (Object collider2 : stonesGroundL)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesGroundL.erase(itStoneGroundL);
				bulletsMagnum.erase(itBullet);
				bulletsOnScreen2--;
				k = false;
			}
			else
			itStoneGroundL++;
		}
		vector<Object>::iterator itStoneGroundR = stonesGroundR.begin();
		for (Object collider2 : stonesGroundR)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesGroundR.erase(itStoneGroundR);
				bulletsMagnum.erase(itBullet);
				bulletsOnScreen2--;
				k = false;
			}
			else
			itStoneGroundR++;
		}
		if (k)
		itBullet++;
	}
	itBullet = bulletsBoost.begin();
	for (Object collider1 : bulletsBoost)
	{
		k = true;
		vector<Object>::iterator itStoneSteelL = stonesSteelL.begin();
		for (Object collider2 : stonesSteelL)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesSteelL.erase(itStoneSteelL);
				bulletsBoost.erase(itBullet);
				bulletsOnScreen3--;
				k = false;
			}
			else
			itStoneSteelL++;
		}
		vector<Object>::iterator itStoneSteelR = stonesSteelR.begin();
		for (Object collider2 : stonesSteelR)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesSteelR.erase(itStoneSteelR);
				bulletsBoost.erase(itBullet);
				bulletsOnScreen3--;
				k = false;
			}
			else
			itStoneSteelR++;
		}
		vector<Object>::iterator itStoneGroundL = stonesGroundL.begin();
		for (Object collider2 : stonesGroundL)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesGroundL.erase(itStoneGroundL);
				bulletsBoost.erase(itBullet);
				bulletsOnScreen3--;
				k = false;
			}
			else
			itStoneGroundL++;
		}
		vector<Object>::iterator itStoneGroundR = stonesGroundR.begin();
		for (Object collider2 : stonesGroundR)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesGroundR.erase(itStoneGroundR);
				bulletsBoost.erase(itBullet);
				bulletsOnScreen3--;
				k = false;
			}
			else
			itStoneGroundR++;
		}
		if (k)
		itBullet++;
	}

	itBullet = bulletsPlayer2.begin();
	for (Object collider1 : bulletsPlayer2)
	{
		k = true;
		vector<Object>::iterator itStoneSteelL = stonesSteelL.begin();
		for (Object collider2 : stonesSteelL)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesSteelL.erase(itStoneSteelL);
				bulletsPlayer2.erase(itBullet);
				bulletsOnScreenPlayer2--;
				k = false;
			}
			else
				itStoneSteelL++;
		}
		vector<Object>::iterator itStoneSteelR = stonesSteelR.begin();
		for (Object collider2 : stonesSteelR)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesSteelR.erase(itStoneSteelR);
				bulletsPlayer2.erase(itBullet);
				bulletsOnScreenPlayer2--;
				k = false;
			}
			else
				itStoneSteelR++;
		}
		vector<Object>::iterator itStoneGroundL = stonesGroundL.begin();
		for (Object collider2 : stonesGroundL)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesGroundL.erase(itStoneGroundL);
				bulletsPlayer2.erase(itBullet);
				bulletsOnScreenPlayer2--;
				k = false;
			}
			else
				itStoneGroundL++;
		}
		vector<Object>::iterator itStoneGroundR = stonesGroundR.begin();
		for (Object collider2 : stonesGroundR)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesGroundR.erase(itStoneGroundR);
				bulletsPlayer2.erase(itBullet);
				bulletsOnScreenPlayer2--;
				k = false;
			}
			else
				itStoneGroundR++;
		}
		if (k)
			itBullet++;
	}

	itBullet = bulletsBoostPlayer2.begin();
	for (Object collider1 : bulletsBoostPlayer2)
	{
		k = true;
		vector<Object>::iterator itStoneSteelL = stonesSteelL.begin();
		for (Object collider2 : stonesSteelL)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesSteelL.erase(itStoneSteelL);
				bulletsBoostPlayer2.erase(itBullet);
				bulletsOnScreen2Player2--;
				k = false;
			}
			else
				itStoneSteelL++;
		}
		vector<Object>::iterator itStoneSteelR = stonesSteelR.begin();
		for (Object collider2 : stonesSteelR)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesSteelR.erase(itStoneSteelR);
				bulletsBoostPlayer2.erase(itBullet);
				bulletsOnScreen2Player2--;
				k = false;
			}
			else
				itStoneSteelR++;
		}
		vector<Object>::iterator itStoneGroundL = stonesGroundL.begin();
		for (Object collider2 : stonesGroundL)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesGroundL.erase(itStoneGroundL);
				bulletsBoostPlayer2.erase(itBullet);
				bulletsOnScreen2Player2--;
				k = false;
			}
			else
				itStoneGroundL++;
		}
		vector<Object>::iterator itStoneGroundR = stonesGroundR.begin();
		for (Object collider2 : stonesGroundR)
		{
			if (collision(collider1, collider2))
			{
				explosion[0].x = collider2.x - 90;
				explosion[0].y = collider2.y - 80;
				stonesGroundR.erase(itStoneGroundR);
				bulletsBoostPlayer2.erase(itBullet);
				bulletsOnScreen2Player2--;
				k = false;
			}
			else
				itStoneGroundR++;
		}
		if (k)
			itBullet++;
	}

		if (boostCollision(spaceship[Player1ship], boostObj[boostRandomizer]))
		{
			switch (boostRandomizer)
			{
			case 0:
				player1.attack = true;
				break;
			case 1:
				player1.defense = true;
				player1.def += 3;
				break;
			case 2:
				player1.speed = true;
				speedPlayer1++;
				if (spaceship[Player1ship].dx < 22)
				{
					spaceship[Player1ship].dx += 3;
					spaceship[Player1ship].dy += 3;
				}				break;
			case 3:
				player1.shield = true;
				shieldPlayer1 = 7;
				break;
			case 4:
				player1.health = true;
				player1.numOfGreenSquares += 2;
				break;
			default:
				cout << "nestonesonesotosenseo";
			}
			boostObj[boostRandomizer].y = -100;
			boostOnScreen = false;
		}

		if (boostCollision(spaceship[Player2ship], boostObj[boostRandomizer]))
		{
			switch (boostRandomizer)
			{
			case 0:
				player2.attack = true;
				break;
			case 1:
				player2.defense = true;
				player2.def += 3;
				break;
			case 2:
				player2.speed = true;
				speedPlayer1++;
				if (spaceship[Player2ship].dx < 22)
				{
					spaceship[Player2ship].dx += 3;
					spaceship[Player2ship].dy += 3;
				}
				break;
			case 3:
				player2.shield = true;
				shieldPlayer2 = 7;
				break;
			case 4:
				player2.health = true;
				player2.numOfGreenSquares += 2;
				break;
			default:
				cout << "nestonesonesotosenseo";
			}
			boostObj[boostRandomizer].y = -100;
			boostOnScreen = false;
		}

		it = stonesSteelR.begin();
		for (Object collider : stonesSteelR)
		{
			if (collision(shieldOnPlayer1, collider))
			{
				explosion[0].x = collider.x - 90;
				explosion[0].y = collider.y - 80;
				stonesSteelR.erase(it);
				shieldPlayer1--;
			}
			else
				it++;
		}
		it = stonesSteelL.begin();
		for (Object collider : stonesSteelL)
		{
			if (collision(shieldOnPlayer1, collider))
			{
				explosion[0].x = collider.x - 90;
				explosion[0].y = collider.y - 80;
				stonesSteelL.erase(it);
				shieldPlayer1--;
			}
			else
				it++;
		}
		it = stonesGroundL.begin();
		for (Object collider : stonesGroundL)
		{

			if (collision(shieldOnPlayer1, collider))
			{
				explosion[0].x = collider.x - 90;
				explosion[0].y = collider.y - 80;
				stonesGroundL.erase(it);
				shieldPlayer1--;
			}
			else
				it++;
		}
		it = stonesGroundR.begin();
		for (Object collider : stonesGroundR)
		{
			if (collision(shieldOnPlayer1, collider))
			{
				explosion[0].x = collider.x - 90;
				explosion[0].y = collider.y - 80;
				stonesGroundR.erase(it);
				shieldPlayer1--;
			}
			else
				it++;
		}
		it = stonesSteelR.begin();
		for (Object collider : stonesSteelR)
		{
			if (collision(shieldOnPlayer2, collider))
			{
				explosion[0].x = collider.x - 90;
				explosion[0].y = collider.y - 80;
				stonesSteelR.erase(it);
				shieldPlayer2--;
			}
			else
				it++;
		}
		it = stonesSteelL.begin();
		for (Object collider : stonesSteelL)
		{
			if (collision(shieldOnPlayer2, collider))
			{
				explosion[0].x = collider.x - 90;
				explosion[0].y = collider.y - 80;
				stonesSteelL.erase(it);
				shieldPlayer2--;
			}
			else
				it++;
		}
		it = stonesGroundL.begin();
		for (Object collider : stonesGroundL)
		{

			if (collision(shieldOnPlayer2, collider))
			{
				explosion[0].x = collider.x - 90;
				explosion[0].y = collider.y - 80;
				stonesGroundL.erase(it);
				shieldPlayer2--;
			}
			else
				it++;
		}
		it = stonesGroundR.begin();
		for (Object collider : stonesGroundR)
		{
			if (collision(shieldOnPlayer2, collider))
			{
				explosion[0].x = collider.x - 90;
				explosion[0].y = collider.y - 80;
				stonesGroundR.erase(it);
				stoneSteelCounterRight--;
				shieldPlayer2--;
			}
			else
				it++;
		}
}

int WINAPI WinMain(HINSTANCE hThisInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpszArgument,
	int nCmdShow)
{

	PlaySound("razor.wav", GetModuleHandle(NULL), SND_FILENAME | SND_ASYNC | SND_LOOP | SND_NOSTOP);
	HWND hwnd;               /* This is the handle for our window */
	MSG messages;            /* Here messages to the application are saved */
	WNDCLASSEX wincl;        /* Data structure for the windowclass */
	TCHAR szClassName[] = _T("CodeBlocksWindowsApp");
	LPCWSTR message = L"My First Game";
	/* The Window structure */
	wincl.hInstance = hThisInstance;
	wincl.lpszClassName = szClassName;
	wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
	wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
	wincl.cbSize = sizeof(WNDCLASSEX);


	/* Use default icon and mouse-pointer */
	wincl.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wincl.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wincl.hCursor = LoadCursor(NULL, IDC_ARROW);
	wincl.lpszMenuName = NULL;                 /* No menu */
	wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
	wincl.cbWndExtra = 0;                      /* structure or the window instance */
											   /* Use Windows's default colour as the background of the window */
	wincl.hbrBackground = (HBRUSH)COLOR_BACKGROUND;


	/* Register the window class, and if it fails quit the program */
	if (!RegisterClassEx(&wincl))
		return 0;



	/* The class is registered, let's create the program*/
	hwnd = CreateWindowEx(
		0,                   /* Extended possibilites for variation */
		szClassName,         /* Classname */
		_T(""),       /* Title Text */
		WS_OVERLAPPED, /* default window */
		CW_USEDEFAULT,       /* Windows decides the position */
		CW_USEDEFAULT,       /* where the window ends up on the screen */
		1920,             /* The programs width */
		1080,             /* and height in pixels */
		HWND_DESKTOP,        /* The window is a child-window to desktop */
		NULL,                /* No menu */
		hThisInstance,       /* Program Instance handler */
		NULL                 /* No Window Creation data */
	);


	/*
	the window visible on the screen */
	//PlaySound("ItemPickup.wav", NULL, SND_FILENAME | SND_LOOP | SND_ASYNC);
	ShowWindow(hwnd, SW_SHOWMAXIMIZED);
	//HBITMAP hbitmap = (HBITMAP)LoadImage(hThisInstance, "home.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	for (int levelNum = 0; levelNum < 7; levelNum++)
	{
		levels[levelNum].lowerBoundTime = 3000 - levelNum * 100;
		levels[levelNum].upperBoundTime = 50000 - levelNum * 200;
		levels[levelNum].lowerBoundSpeed = 5 + levelNum * 2;
		levels[levelNum].upperBoundSpeed = 15 + levelNum * 4;
		levels[levelNum].timeDuration = 60000 + levelNum * 10000;
	}

	if (Initialize())
	{
		while (exitMarker)
		{
			DWORD vrijeme_pocetak;

			if (PeekMessage(&messages, NULL, 0, 0, PM_REMOVE))
			{
				if (messages.message == WM_QUIT)
				{
					break;
				}
				TranslateMessage(&messages);
				DispatchMessage(&messages);
			}
			vrijeme_pocetak = GetTickCount();
			CheckInput(hwnd);
			UpdateGamePlay(hwnd);
			Render(hwnd);

			while ((GetTickCount() - vrijeme_pocetak) < PAUZA)
			{
				Sleep(5);
			}
		}
	}

	/* Run the message loop. It will run until GetMessage() returns 0 */
	while (GetMessage(&messages, NULL, 0, 0))
	{
		/* Translate virtual-key messages into character messages */
		TranslateMessage(&messages);
		/* Send message to WindowProcedure */
		DispatchMessage(&messages);
	}

	/* The program return-value is 0 - The value that PostQuitMessage() gave */
	//t.join();
	return messages.wParam;

}


/*  This function is called by the Windows function DispatchMessage()  */

LRESULT CALLBACK WindowProcedure(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)                  /* handle the messages */
	{
	case WM_DESTROY:

		PostQuitMessage(0);       /* send a WM_QUIT to the message queue */
		break;
	case WM_CLOSE:

		DestroyWindow(hwnd);
		return 0;
	default:                      /* for messages that we don't deal with */
		return DefWindowProc(hwnd, message, wParam, lParam);
	}


	return 0;
}

void CheckInput(HWND hwnd)
{


	if (KEYDOWN(VK_UP))
	{
		if (glBackGround > 28 && glBackGround < 36)
		{
			if (spaceship[Player1ship].y>300)
				spaceship[Player1ship].y -= spaceship[Player1ship].dy;
		}
		else if (GetTickCount() - pocvr > 50)
		{
			PlaySound("Blip_Select19.wav", NULL, SND_FILENAME | SND_ASYNC | SND_NOSTOP);

			switch (glBackGround)
			{
			case 0:
				glBackGround = BGPLAYHOVER;
				break;
			case 1:
				glBackGround = BGMAINHOVER;
				break;
			case 2:
				glBackGround = BG1PHOVER;
				break;
			case 3:
				glBackGround = BGCTRLHOVER;
				break;
			case 4:
				glBackGround = BGLOADHOVER;
				break;
			case 5:
				glBackGround = BGPLAYHOVER;
				break;
			case 6:
				glBackGround = BGCREDHOVER;
				break;
			case 9:
				glBackGround = BGEXITHOVER;
				break;
			case 10:
				glBackGround = BG1PHOVER;
				break;
			case 11:
				glBackGround = BG2PHOVER;
				break;
			case 12:
				glBackGround = BGSHIPHOVER;
				break;
			case 13:
				glBackGround = BGSAVEHOVER;
				break;
			case 14:
				glBackGround = BGPSHIPHOVER;
				break;
			case 15:
				glBackGround = BGPMAINHOVER;
				break;
			case 16:
				glBackGround = BGPSHIPHOVER;
				break;
			case 17:
				glBackGround = BGPSAVEHOVER;
				break;
			case 18:
				glBackGround = BGMARVEL;
				break;
			case 19:
				glBackGround = BGMDZ;
				break;
			case 20:
				glBackGround = BGCRR;
				break;
			case 21:
				glBackGround = BGMGNM;
				break;
			case 22:
				glBackGround = BGBUG;
				break;
			case 23:
				glBackGround = BGSHARP;
				break;
			case 24:
				glBackGround = BGEJ;
				break;
			case 25:
				glBackGround = BGTUZLA;
				break;
			case 26:
				glBackGround = BGFETM;
				break;
			case 27:
				glBackGround = BGFETE;
				break;
			default:
				cout << "jao";
			}

		}
		pocvr = GetTickCount();
	}

	if (KEYDOWN(VK_DOWN))
	{
		if (glBackGround > 28 && glBackGround < 36)
		{
			if (spaceship[Player1ship].y < (1080 - spaceship[Player1ship].height))
				spaceship[Player1ship].y += spaceship[Player1ship].dy;
		}
		else if (GetTickCount() - pocvr > 50)
		{
			PlaySound("Blip_Select19.wav", NULL, SND_FILENAME | SND_ASYNC | SND_NOSTOP);


			switch (glBackGround)
			{
			case 0:
				glBackGround = BGPLAYHOVER;
				break;
			case 1:
				glBackGround = BG2PHOVER;
				break;
			case 2:
				glBackGround = BGSHIPHOVER;
				break;
			case 3:
				glBackGround = BGEXITHOVER;
				break;
			case 4:
				glBackGround = BGCREDHOVER;
				break;
			case 5:
				glBackGround = BGCTRLHOVER;
				break;
			case 6:
				glBackGround = BGPLAYHOVER;
				break;
			case 9:
				glBackGround = BGLOADHOVER;
				break;
			case 10:
				glBackGround = BG1PHOVER;
				break;
			case 11:
				glBackGround = BGSAVEHOVER;
				break;
			case 12:
				glBackGround = BGMAINHOVER;
				break;
			case 13:
				glBackGround = BG1PHOVER;
				break;
			case 14:
				glBackGround = BGPSHIPHOVER;
				break;
			case 15:
				glBackGround = BGPSAVEHOVER;
				break;
			case 16:
				glBackGround = BGPMAINHOVER;
				break;
			case 17:
				glBackGround = BGPSHIPHOVER;
				break;
			case 18:
				glBackGround = BGCRR;
				break;
			case 19:
				glBackGround = BGMGNM;
				break;
			case 20:
				glBackGround = BGBUG;
				break;
			case 21:
				glBackGround = BGSHARP;
				break;
			case 22:
				glBackGround = BGEJ;
				break;
			case 23:
				glBackGround = BGTUZLA;
				break;
			case 24:
				glBackGround = BGFETM;
				break;
			case 25:
				glBackGround = BGFETE;
				break;
			case 26:
				glBackGround = BGMARVEL;
				break;
			case 27:
				glBackGround = BGMDZ;
				break;
			default:
				cout << "jao";
			}

		}
		pocvr = GetTickCount();
	}

	if (KEYDOWN(VK_RETURN))
	{
		if (GetTickCount() - pocvr > 50)
		{
			PlaySound("Blip_Select19.wav", NULL, SND_FILENAME | SND_ASYNC | SND_NOSTOP);
			switch (glBackGround)
			{
			case 1:
				glBackGround = 29 + currentLevel;
				glPlayers = 1;
				break;
			case 2:
				glBackGround = 29 + currentLevel;
				glPlayers = 2;
				break;
			case 3:
				glBackGround = BGCRED;
				break;
			case 4:
				glBackGround = BGCTRL;
				break;
			case 5:
				glBackGround = BGSTART;
				if (load.is_open())
				{
					getline(load, line);
				}
				istringstream(line)>>currentLevel;
				break;
			case 6:
				DestroyWindow(hwnd);
				exitMarker = false;
				break;
			case 9:
				glBackGround = BGPLAY;
				break;
			case 11:
				glBackGround = BGMDZ;
				break;
			case 12:
				glBackGround = BGPLAY;
				save.open("save.txt");
				save << currentLevel;
				save.close();
				break;
			case 13:
				glBackGround = BGSTART;
				break;
			case 15:
				glBackGround = BGMDZ;
				break;
			case 16:
				glBackGround = BGPAUSE;
				save.open("save.txt");
				save << currentLevel;
				save.close();
				break;
			case 17:
				glBackGround = BGSTART;
				break;
			case 18:
				glBackGround = BGPSHIPHOVER;
				break;
			case 19:
				glBackGround = BGPSHIPHOVER;
				break;
			case 20:
				glBackGround = BGPSHIPHOVER;
				break;
			case 21:
				glBackGround = BGPSHIPHOVER;
				break;
			case 22:
				glBackGround = BGPSHIPHOVER;
				break;
			case 23:
				glBackGround = BGPSHIPHOVER;
				break;
			case 24:
				glBackGround = BGPSHIPHOVER;
				break;
			case 25:
				glBackGround = BGPSHIPHOVER;
				break;
			case 26:
				glBackGround = BGPSHIPHOVER;
				break;
			case 27:
				glBackGround = BGPSHIPHOVER;
				break;
			case 28:
				bossDead=false;
				currentLevel++;
				if (currentLevel == 6)
					currentLevel = 0;
				glBackGround=glBackGround+currentLevel+1;
				boss[0].y = 10; 
				spaceship[Player1ship].y = 500;
				spaceship[Player2ship].y = 500;
				boss[0].numOfGreenSquares = (1+currentLevel) * 10;
				boss[0].dx++;
				break;
			case 36:
				bossDead = false;
				glBackGround = 29;
				currentLevel = 0;
				boss[0].numOfGreenSquares = (1 + currentLevel) * 10;
				boss[0].x = 500;
				player1.numOfGreenSquares = 10;
				player2.numOfGreenSquares = 10;
				spaceship[Player1ship].x = 500;
				spaceship[Player1ship].y = 800;
				spaceship[Player2ship].x = 800;
				spaceship[Player2ship].y = 800;
			default:
				cout << "jao";
			}

		}
		pocvr = GetTickCount();
	}
	if (KEYDOWN(VK_ESCAPE))
	{
		if (GetTickCount() - pocvr > 50)
		{
			PlaySound("Blip_Select19.wav", NULL, SND_FILENAME | SND_ASYNC | SND_NOSTOP);
			switch (glBackGround)
			{


			case 7:
				glBackGround = BGCTRLHOVER;
				break;
			case 8:
				glBackGround = BGCREDHOVER;
				break;
			case 14:
				glBackGround = 29+currentLevel;
				break;
			case 15:
				glBackGround = 29+currentLevel;
				break;
			case 16:
				glBackGround = 29 + currentLevel;
				break;
			case 17:
				glBackGround = 29 + currentLevel;
				break;
			case 18:
				glBackGround = BGPLAY;
				break;
			case 19:
				glBackGround = BGPLAY;
				break;
			case 20:
				glBackGround = BGPLAY;
				break;
			case 21:
				glBackGround = BGPLAY;
				break;
			case 22:
				glBackGround = BGPLAY;
				break;
			case 23:
				glBackGround = BGPLAY;
				break;
			case 24:
				glBackGround = BGPLAY;
				break;
			case 25:
				glBackGround = BGPLAY;
				break;
			case 26:
				glBackGround = BGPLAY;
				break;
			case 27:
				glBackGround = BGPLAY;
				break;
			case 29:
				glBackGround = BGPAUSE;
				break;
			case 30:
				glBackGround = BGPAUSE;
				break;
			case 31:
				glBackGround = BGPAUSE;
				break;
			case 32:
				glBackGround = BGPAUSE;
				break;
			case 33:
				glBackGround = BGPAUSE;
				break;
			case 34:
				glBackGround = BGPAUSE;
				break;
			case 35:
				glBackGround = BGPAUSE;
				break;
			default:
				cout << "jao";
			}

		}
		pocvr = GetTickCount();
	}
	if (KEYDOWN(VK_NUMPAD2))
	{
		if (GetTickCount() - pocvr > 50)
		{
			switch (glBackGround)
			{
			case 18:
				Player2ship = 0;
				break;
			case 19:
				Player2ship = 1;
				break;
			case 20:
				Player2ship = 2;
				break;
			case 21:
				Player2ship = 3;
				break;
			case 22:
				Player2ship = 4;
				break;
			case 23:
				Player2ship = 5;
				break;
			case 24:
				Player2ship = 6;
				break;
			case 25:
				Player2ship = 7;
				break;
			case 26:
				Player2ship = 8;
				break;
			case 27:
				Player2ship = 9;
				break;
			default:
				cout << "dkjsajdias" << endl;
			}

		}
		changedShip2 = true;
		pocvr = GetTickCount();
	}
	if (KEYDOWN(VK_NUMPAD1))
	{
		if (GetTickCount() - pocvr > 50)
		{
			switch (glBackGround)
			{
			case 18:
				Player1ship = 0;
				break;
			case 19:
				Player1ship = 1;
				break;
			case 20:
				Player1ship = 2;
				break;
			case 21:
				Player1ship = 3;
				break;
			case 22:
				Player1ship = 4;
				break;
			case 23:
				Player1ship = 5;
				break;
			case 24:
				Player1ship = 6;
				break;
			case 25:
				Player1ship = 7;
				break;
			case 26:
				Player1ship = 8;
				break;
			case 27:
				Player1ship = 9;
				break;
			default:
				cout << "dkjsajdias" << endl;
			}

		}
		changedShip1 = true;
		pocvr = GetTickCount();
	}
	if (KEYDOWN(VK_NUMPAD7))
	{
		PAUZA = 30;
	}
	if (KEYDOWN(VK_NUMPAD8))
	{
		PAUZA = 20;
	}
	if (KEYDOWN(VK_NUMPAD9))
	{
		PAUZA = 0;
	}
	if (KEYDOWN(VK_LEFT))
	{
		if (spaceship[Player1ship].x>0)
			spaceship[Player1ship].x -= spaceship[Player1ship].dx;
	}
	if (KEYDOWN(VK_RIGHT))
	{
		if (spaceship[Player1ship].x<(1920 - spaceship[Player1ship].width))
			spaceship[Player1ship].x += spaceship[Player1ship].dx;
	}
	if (KEYDOWN(0x44))
	{
		if (spaceship[Player2ship].x<(1920 - spaceship[Player2ship].width))
			spaceship[Player2ship].x += spaceship[Player2ship].dx;

	}
	if (KEYDOWN(0x41))
	{
		if (spaceship[Player2ship].x>0)
			spaceship[Player2ship].x -= spaceship[Player2ship].dx;

	}
	if (KEYDOWN(0x57))
	{
		if (spaceship[Player2ship].y > 300)
			spaceship[Player2ship].y -= spaceship[Player2ship].dy;

	}
	if (KEYDOWN(0x53))
	{
		if (spaceship[Player2ship].y<1080 - spaceship[Player2ship].height)
			spaceship[Player2ship].y += spaceship[Player2ship].dy;

	}

	if (KEYDOWN(VK_SPACE))
	{
		if (GetTickCount() - pocvr > 250)
		{
			if (Player2ship == 2)
			{
				bulletsOnScreen2++;
				bulletMagnum[bulletCounter2].x = spaceship[Player2ship].x + spaceship[Player2ship].width / 2 - bulletMagnum[bulletCounter2].width / 2;
				bulletMagnum[bulletCounter2].y = spaceship[Player2ship].y - bulletMagnum[bulletCounter2].height;
			}
			else if (player2.attack)
			{
				bulletsOnScreen2Player2++;
				bulletBoostPlayer2[bulletCounter2Player2].x = spaceship[Player2ship].x + spaceship[Player2ship].width / 2 - bulletBoostPlayer2[bulletCounter2Player2].width / 2;
				bulletBoostPlayer2[bulletCounter2Player2].y = spaceship[Player2ship].y - bulletBoostPlayer2[bulletCounter2Player2].height;
			}
			else
			{
				bulletsOnScreenPlayer2++;
				bulletPlayer2[bulletCounterPlayer2].x = spaceship[Player2ship].x + spaceship[Player2ship].width / 2 - bulletPlayer2[bulletCounterPlayer2].width / 2;
				bulletPlayer2[bulletCounterPlayer2].y = spaceship[Player2ship].y - bulletPlayer2[bulletCounterPlayer2].height;
			}
			pocvr = GetTickCount();

		}

	}

	if (KEYDOWN(0x4F))
	{
		if (GetTickCount() - pocvr2 > 250)
		{
			if (Player1ship == 2)
			{
				bulletsOnScreen2++;
				bulletMagnum[bulletCounter2].x = spaceship[Player1ship].x + spaceship[Player1ship].width / 2 - bulletMagnum[bulletCounter2].width / 2;
				bulletMagnum[bulletCounter2].y = spaceship[Player1ship].y - bulletMagnum[bulletCounter2].height;
			}
			else if (player1.attack)
			{
				bulletsOnScreen3++;
				bulletBoost[bulletCounter3].x = spaceship[Player1ship].x + spaceship[Player1ship].width / 2 - bulletBoost[bulletCounter3].width / 2;
				bulletBoost[bulletCounter3].y = spaceship[Player1ship].y - bulletBoost[bulletCounter3].height;
			}
			else
			{
				bulletsOnScreen++;
				bullet[bulletCounter].x = spaceship[Player1ship].x + spaceship[Player1ship].width / 2 - bullet[bulletCounter].width / 2;
				bullet[bulletCounter].y = spaceship[Player1ship].y - bullet[bulletCounter].height;
			}
			pocvr2 = GetTickCount();

		}

	}

}

void UpdateGamePlay(HWND hwnd)
{
	if (glBackGround >= 28 && glBackGround < 36)
	{
		bossAI();


		if (boostOnScreen)
		{
			boostObj[boostRandomizer].y += 10;
			if (boostObj[boostRandomizer].y > 1080)
			{
				boostOnScreen = false;
				boostObj[boostRandomizer].x = rand() % 1920;
				boostObj[boostRandomizer].y = -100;
			}
		}

		if (GetTickCount() - bossTimer > 500)
		{
			bossBulletsOnScreen++;
			bossBullet[bossBulletCounter].x = boss[0].x + boss[0].width / 2 - bossBullet[bossBulletCounter].width / 2;
			bossBullet[bossBulletCounter].y = boss[0].y + boss[0].height;
			bossTimer = GetTickCount();
		}

		if (bossBulletCounter > 9)
			bossBulletCounter = 0;

		for (int i = 0; i < bossBullets.size(); i++)
		{
			bossBullets[i].y += bossBullets[i].dy;
			if (bossBullets[i].y > 1080)
			{
				bossBullets.erase(begin(bossBullets));
				bossBulletsOnScreen--;
			}
		}

		if (changedShip1 || hasNotBeenSet1)
		{
			switch (Player1ship)
			{
			case 0:
				player1.numOfGreenSquares = 10;
				player1.def = 3;
				break;
			case 1:
				player1.numOfGreenSquares = 12;
				player1.def = 4;
				break;
			case 2:
				player1.numOfGreenSquares = 6;
				player1.def = 2;
				break;
			case 3:
				player1.numOfGreenSquares = 6;
				player1.def = 2;
				break;
			case 4:
				player1.numOfGreenSquares = 10;
				player1.def = 6;
				break;
			case 5:
				player1.numOfGreenSquares = 10;
				player1.def = 3;
				break;
			case 6:
				player1.numOfGreenSquares = 10;
				player1.def = 3;
				break;
			case 7:
				player1.numOfGreenSquares = 6;
				player1.def = 2;
				break;
			case 8:
				player1.numOfGreenSquares = 10;
				player1.def = 3;
				break;
			case 9:
				player1.numOfGreenSquares = 10;
				player1.def = 3;
				break;
			default:
				break;
			}
			changedShip1 = false;
			hasNotBeenSet1 = false;
		}
		if (glPlayers == 2)
		{
			if (changedShip2 || hasNotBeenSet2)
			{
				switch (Player2ship)
				{
				case 0:
					player2.numOfGreenSquares = 10;
					player2.def = 3;
					break;
				case 1:
					player2.numOfGreenSquares = 12;
					player2.def = 4;
					break;
				case 2:
					player2.numOfGreenSquares = 6;
					player2.def = 2;
					break;
				case 3:
					player2.numOfGreenSquares = 6;
					player2.def = 2;
					break;
				case 4:
					player2.numOfGreenSquares = 10;
					player2.def = 6;
					break;
				case 5:
					player2.numOfGreenSquares = 10;
					player2.def = 3;
					break;
				case 6:
					player2.numOfGreenSquares = 10;
					player2.def = 3;
					break;
				case 7:
					player2.numOfGreenSquares = 6;
					player2.def = 2;
					break;
				case 8:
					player2.numOfGreenSquares = 10;
					player2.def = 3;
					break;
				case 9:
					player2.numOfGreenSquares = 10;
					player2.def = 3;
					break;
				default:
					break;
				}
				changedShip2 = false;
				hasNotBeenSet2 = false;
			}
		}
		else {
			player2.numOfGreenSquares = 0;
			spaceship[Player2ship].y = -100000;
		}

		if (stoneSteelCounterLeft > 24)
			stoneSteelCounterLeft = 0;
		for (int counter = 0; counter < stonesSteelL.size(); counter++)
		{
			stonesSteelL[counter].x += stonesSteelL[counter].dx;
			stonesSteelL[counter].y += stonesSteelL[counter].dy;
			if (stonesSteelL[counter].y > 1080)
			{
				stonesSteelL.erase(begin(stonesSteelL));
			}
		}
		if (stoneSteelCounterRight > 24)
			stoneSteelCounterRight = 0;
		for (int counter = 0; counter < stonesSteelR.size(); counter++)
		{
			stonesSteelR[counter].x -= stonesSteelR[counter].dx;
			stonesSteelR[counter].y += stonesSteelR[counter].dy;
			if (stonesSteelR[counter].y > 1080)
			{
				stonesSteelR.erase(begin(stonesSteelR));
			}
		}
		if (stoneGroundCounterRight > 24)
			stoneGroundCounterRight = 0;
		for (int counter = 0; counter < stonesGroundR.size(); counter++)
		{
			stonesGroundR[counter].x -= stonesGroundR[counter].dx;
			stonesGroundR[counter].y += stonesGroundR[counter].dy;
			if (stonesGroundR[counter].y > 1080)
			{
				stonesGroundR.erase(begin(stonesGroundR));
			}
		}
		if (stoneGroundCounterLeft > 24)
			stoneGroundCounterLeft = 0;
		for (int counter = 0; counter < stonesGroundL.size(); counter++)
		{
			stonesGroundL[counter].x += stonesGroundL[counter].dx;
			stonesGroundL[counter].y += stonesGroundL[counter].dy;
			if (stonesGroundL[counter].y > 1080)
			{
				stonesGroundL.erase(begin(stonesGroundL));
			}
		}
		if (bulletCounter > 9)
			bulletCounter = 0;

		for (int i = 0; i < bullets.size(); i++)
		{
			bullets[i].y -= bullets[i].dy;
			if (bullets[i].y + bullets[i].height < 0)
			{
				bullets.erase(begin(bullets));
				bulletsOnScreen--;
			}
		}

		if (bulletCounter2 > 9)
			bulletCounter2 = 0;

		for (int q = 0; q < bulletsMagnum.size(); q++)
		{
			bulletsMagnum[q].y -= bulletsMagnum[q].dy;
			if (bulletsMagnum[q].y + bulletsMagnum[q].height < 0)
			{
				bulletsMagnum.erase(begin(bulletsMagnum));
				bulletsOnScreen2--;
			}
		}

		if (bulletCounter3 > 9)
			bulletCounter3 = 0;

		for (int i = 0; i < bulletsBoost.size(); i++)
		{
			bulletsBoost[i].y += bulletsBoost[i].dy;
			if (bulletsBoost[i].y + bulletsBoost[i].height < 0)
			{
				bulletsBoost.erase(begin(bulletsBoost));
				bulletsOnScreen3--;
			}
		}

		checkForCollisions();

		if (markerForGetTickCountSR)
		{
			timeSinceStoneSR = GetTickCount();
			markerForGetTickCountSR = false;
		}

		if (GetTickCount() - timeSinceStoneSR > rand() % (levels[currentLevel].upperBoundTime - levels[currentLevel].lowerBoundTime) + levels[currentLevel].lowerBoundTime) {
			stonesSteel[stoneSteelCounterRight].x = 1420 + rand() % 750;
			stonesSteelR.push_back(stonesSteel[stoneSteelCounterRight]);
			markerForGetTickCountSR = true;
		}

		if (markerForGetTickCountSL)
		{
			timeSinceStoneSL = GetTickCount();
			markerForGetTickCountSL = false;
		}

		if (GetTickCount() - timeSinceStoneSL > rand() % (levels[currentLevel].upperBoundTime - levels[currentLevel].lowerBoundTime) + levels[currentLevel].lowerBoundTime) {
			stonesSteel[stoneSteelCounterLeft].x = -150 + rand() % 750;
			stonesSteelL.push_back(stonesSteel[stoneSteelCounterLeft]);
			markerForGetTickCountSL = true;
		}

		if (markerForGetTickCountGR)
		{
			timeSinceStoneGR = GetTickCount();
			markerForGetTickCountGR = false;
		}

		if (GetTickCount() - timeSinceStoneGR > rand() % levels[currentLevel].upperBoundTime) {
			stonesGround[stoneGroundCounterRight].x = 1420 + rand() % 750;
			stonesGroundR.push_back(stonesGround[stoneGroundCounterRight]);
			markerForGetTickCountGR = true;
		}

		if (markerForGetTickCountGL)
		{
			timeSinceStoneGL = GetTickCount();
			markerForGetTickCountGL = false;
		}


		if (GetTickCount() - timeSinceStoneGL > rand() % levels[currentLevel].upperBoundTime) {
			stonesGround[stoneGroundCounterLeft].x = -120 + rand() % 750;
			stonesGroundL.push_back(stonesGround[stoneGroundCounterLeft]);
			markerForGetTickCountGL = true;
		}

		for (int count = 0; count < player1.numOfGreenSquares; count++)
		{
			green[count].x = 50 + (count + 1)*green[count].width;
			green[count].y = 26;

		}
		for (int count = 0; count < player2.numOfGreenSquares; count++)
		{
			green2[count].x = 1850 - (count + 1)*green2[count].width;
			green2[count].y = 26;
		}

		if (bossDead)
		{
			spaceship[Player1ship].y -= 10;
			spaceship[Player2ship].y -= 10;
			if (spaceship[Player1ship].y < -500 && spaceship[Player2ship].y < -500)
				glBackGround = 28;
		}
		if (glPlayers == 2)
		{
			if (player1.numOfGreenSquares <= 0 && player2.numOfGreenSquares <= 0)
				glBackGround = 36;
		}
		else
		{
			if (player1.numOfGreenSquares <= 0)
				glBackGround = 36;
		}

		if (player1.numOfGreenSquares <= 0)
			spaceship[Player1ship].x = 10000;
		if (player2.numOfGreenSquares <= 0)
			spaceship[Player2ship].x = 10000;
	}

	if (bulletCounterPlayer2 > 9)
		bulletCounterPlayer2 = 0;

	for (int i = 0; i < bulletsPlayer2.size(); i++)
	{
		bulletsPlayer2[i].y -= bulletsPlayer2[i].dy;
		if (bulletsPlayer2[i].y + bulletsPlayer2[i].height < 0)
		{
			bulletsPlayer2.erase(begin(bulletsPlayer2));
			bulletsOnScreenPlayer2--;
		}
	}

	if (bulletCounter2Player2 > 9)
		bulletCounter2Player2 = 0;

	for (int i = 0; i < bulletsBoostPlayer2.size(); i++)
	{
		bulletsBoostPlayer2[i].y -= bulletsBoostPlayer2[i].dy;
		if (bulletsBoostPlayer2[i].y + bulletsBoostPlayer2[i].height < 0)
		{
			bulletsBoostPlayer2.erase(begin(bulletsBoostPlayer2));
			bulletsOnScreen2Player2--;
		}
	}

	if (shieldPlayer1 <= 0)
	{
		player1.shield = false;
		shieldOnPlayer1.x = 40000;
	}
	if (shieldPlayer2 <= 0)
	{
		player2.shield = false;
		shieldOnPlayer2.x = 40000;
	}

	if (player1.shield)
	{
		shieldOnPlayer1.x = spaceship[Player1ship].x - (shieldOnPlayer1.width - spaceship[Player1ship].width) / 2;
		shieldOnPlayer1.y = spaceship[Player1ship].y - (shieldOnPlayer1.height - spaceship[Player1ship].height) / 2;
	}
	if (player2.shield)
	{
		shieldOnPlayer2.x = spaceship[Player2ship].x - (shieldOnPlayer2.width - spaceship[Player2ship].width) / 2;
		shieldOnPlayer2.y = spaceship[Player2ship].y - (shieldOnPlayer2.height - spaceship[Player2ship].height) / 2;
	}
	
	/*if (Player1ship==2)
		if (player1.attack)
			spaceship*/
	if (glBackGround == 28 || glBackGround == 36)
		{
			if (player1.speed)
			{
				spaceship[Player1ship].dx -= speedPlayer1*3;
				spaceship[Player1ship].dy -= speedPlayer1*3;
			}
			if (player2.speed)
			{
				spaceship[Player2ship].dx -= speedPlayer2*3;
				spaceship[Player2ship].dy -= speedPlayer2*3;
			}
			speedPlayer1 = 0;
			speedPlayer2 = 0;
			player1.attack = false;
			player1.defense = false;
			player1.speed = false;
			player1.shield = false;
			player1.health = false;
			player2.attack = false;
			player2.defense = false;
			player2.speed = false;
			player2.shield = false;
			player2.health = false;
		}
}